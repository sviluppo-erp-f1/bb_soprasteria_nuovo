﻿using GestioneCerved;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.VisualBasic.FileIO;
using System.Data;

namespace GestioneCerved
{
    class ImportCerved
    {
        private XmlDocument docXml = new XmlDocument();
        private string path ="";
        SAPbobsCOM.Company  oCompany = new SAPbobsCOM.Company();
 
/*
     public  DataTable GetDataTabletFromCSVFile(string csv_file_path)  
    {
        DataTable csvData = new DataTable();
        try
        {
          using(TextFieldParser csvReader = new TextFieldParser(csv_file_path))
             {
                csvReader.SetDelimiters(new string[] { ";" });
                csvReader.HasFieldsEnclosedInQuotes = true;
                string[] colFields = csvReader.ReadFields();
                foreach (string column in colFields)
                {
                    DataColumn datecolumn = new DataColumn(column);
                    datecolumn.AllowDBNull = true;
                    csvData.Columns.Add(datecolumn);
                }
                while (!csvReader.EndOfData)
                {
                    string[] fieldData = csvReader.ReadFields();
                    //Making empty value as null
                    for (int i = 0; i < fieldData.Length; i++)
                    {
                        if (fieldData[i] == "")
                        {
                            fieldData[i] = null;
                        }
                    }
                    csvData.Rows.Add(fieldData);
                }
            }
        }
        catch (Exception ex)
        {
           return null;
        }
        return csvData;
    }
    */
        private void Connetti() 
        {
            docXml.Load("parametriDB.xml");
            XmlNodeList elemList = docXml.GetElementsByTagName("ParametriConnessioneDB");
            foreach (XmlNode node in elemList)
            {
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    connessioneDB(childNode);
                }
            }


        }

        public bool aggiornaDB() 
        {
            //return false;
            try
            {
                SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT " + '"' + "Name" + '"' + " FROM " + '"' + "@ADDONPAR" + '"' + " WHERE " + '"' + "Code" + '"' + " = 'AGGIORNA_DB'");
                if (oRecordset.Fields.Item(0).Value.ToString() == "S")
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return true;
            }
            return false;
        }

        public void Importa()
        {
            docXml.Load("parametriDB.xml");
            XmlNodeList elemList = docXml.GetElementsByTagName("ParametriConnessioneDB");
            foreach (XmlNode node in elemList)
            {
                foreach (XmlNode childNode in node.ChildNodes)
                {
                    connessioneDB(childNode);
                }
            }


            if (aggiornaDB())
            {
                addonDB MyDB = new addonDB();
                MyDB.Add(oCompany);


                SAPbobsCOM.UserTablesMD oUserTablesMD = (SAPbobsCOM.UserTablesMD)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserTables);
                //La tabella parametri non esiste
                if (oUserTablesMD.GetByKey("ADDONPAR"))
                {
                    SAPbobsCOM.UserTable UTS = (SAPbobsCOM.UserTable)oCompany.UserTables.Item(oUserTablesMD.TableName);
                    if (UTS.GetByKey("AGGIORNA_DB"))
                    {
                        UTS.Name = "N";
                        UTS.Update();
                    }
                    else
                    {
                        UTS.Code = "AGGIORNA_DB";
                        UTS.Name = "N";
                        UTS.Add();
                    }
                }

            }

            //docXml.Load("parametriPATH.xml");

            //elemList = docXml.GetElementsByTagName("ParametriPath");

            //foreach (XmlNode node in elemList)
            //{
            //    foreach (XmlNode childNode in node.ChildNodes)
            //    {
            //        path=childNode.Attributes["path"].Value;

            //    }
            //}
            //try
            //{
            //    Program.log.Info("Inizio inserimento Fido Cerved");
            //    elabora();
            //    Program.log.Info("Fine inserimento Fido Cerved");
            //}
            //catch (Exception e)
            //{
            //    Program.log.Info("Errore in inserimento/aggiornamento articoli: " + e.Message + " - " + e.InnerException);
            //}

            disconnessioneDB();

        }

       

        public void connessioneDB(XmlNode childNode)
        {
            if (!oCompany.Connected)
            {
                oCompany.Server = childNode.Attributes["Server"].Value;
                oCompany.DbUserName = childNode.Attributes["DbUserName"].Value;
                oCompany.DbPassword = childNode.Attributes["DbPassword"].Value;
                oCompany.CompanyDB = childNode.Attributes["CompanyDB"].Value;
                oCompany.UserName = childNode.Attributes["UserName"].Value;
                oCompany.Password = childNode.Attributes["Password"].Value;

                oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                oCompany.UseTrusted = false;

                int errCode = oCompany.Connect();

                if (errCode != 0)
                {
                    string err;
                    int code;
                    oCompany.GetLastError(out code, out err);
                    Program.log.Info("Errore in connessione al db SAP"); 
                }
                else
                {
                    Program.log.Info("Connessione al db di SAP");
                } 
            }
        }

        private void disconnessioneDB()
        {
            if (oCompany.Connected)
            {
                oCompany.Disconnect();
                oCompany = null; 
                Program.log.Info("Disconnessione dal db di SAP");
            }
            
        }

        private void elabora()
        {

            /*
            SAPbobsCOM.UserTable FidoRighe  ;
            string[] files = System.IO.Directory.GetFiles(path, "*.csv");

            foreach (string s in files)
            {
                // Create the FileInfo object only when needed to ensure
                // the information is as current as possible.
                System.IO.FileInfo fi = null;
                try
                {
                    fi = new System.IO.FileInfo(s);
                }
                catch (System.IO.FileNotFoundException e)
                {

                }
            

                DataTable csvData = GetDataTabletFromCSVFile(s);

                SAPbobsCOM.Recordset oRecordset = (SAPbobsCOM.Recordset)oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                oRecordset.DoQuery("SELECT max(" + '"' + "U_PrgAcq" + '"' + "),coalesce(max(cast(" + '"' + "Code" + '"' + "as number)),'0') FROM " + '"' + "@CERVED_FIDO" + '"');

                int prg = 1;
                double code = 1;
               while (!oRecordset.EoF)
           
                {
                    prg = oRecordset.Fields.Item(0).Value + 1;
                    // code = Convert.ToInt32( (oRecordset.Fields.Item(1).Value));
                    code =  (oRecordset.Fields.Item(1).Value);
                    oRecordset.MoveNext();
                }

                //  Get The Last Key and Increase it       

                FidoRighe = oCompany.UserTables.Item("CERVED_FIDO");

                foreach(DataRow row in csvData.Rows)
                    {
                        code+=1;
                        FidoRighe.Code = Convert.ToString( code);
                        FidoRighe.Name = Convert.ToString(code); 
                    //DateTime.Now.ToString("yyyyMMddhmm tt");
                        FidoRighe.UserFields.Fields.Item("U_PrgAcq").Value = prg;
                        FidoRighe.UserFields.Fields.Item("U_NomeFile").Value = fi.Name;
                        FidoRighe.UserFields.Fields.Item("U_CardCode").Value = row["COD_CLIENTE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_IDSoggetto").Value = row["ID_SOGGETTO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_AddId").Value = row["CODICE_FISCALE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_LicTradNum").Value = row["PARTITA_IVA"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_CardName").Value = row["RAGIONE_SOCIALE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_NatGiur").Value = row["NATURA GIURIDICA"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_Street").Value = row["INDIRIZZO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ZipCode").Value = row["CAP"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_City").Value = row["COMUNE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_State").Value = row["PV"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ATECO").Value = row["ATECO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_StatoAttivita").Value = row["STATO_ATTIVITA"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_FidoPayline").Value = row["FIDO_PAYLINE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_Decisione").Value = row["DECISIONE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ValTmpPag").Value = row["VALUTAZIONE_TEMPI_PAGAM"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_GGPattuiti").Value = row["GG_PATTUITI"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_GGRitardo").Value = row["GG_RITARDO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_Fatturato").Value = row["FATTURATO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_FattMese").Value = row["FATTURATO_MESE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_Scaduto").Value = row["SCADUTO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_Esposizione").Value = row["ESPOSIZIONE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ClsRischio").Value = row["CLASSE_RISCHIO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ClsRischioDesc").Value = row["CLASSE_RISCHIO_DESCR"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ScoreSitEcoFinanz").Value = row["SCORE_SITUAZ_ECON_FINANZIARIA"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ScoreEvNeg").Value = row["SCORE_EVENTI_NEGATIVI"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ScoreRischNrConsul").Value = row["SCORE_RISCHIO_NR_CONSULTAZIONI"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ScoreConsImm").Value = row["SCORE_CONSISTENZA_IMMOBILIARE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_ScoreProfStrutt").Value = row["SCORE_PROFILO_STRUTTURALE"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_CGS").Value = row["CGS"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_CGSDesc").Value = row["CGS_DESCR"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_DtScaMonit").Value = row["DATA_SCADENZA_MONITORAGGIO"].ToString();
                        FidoRighe.UserFields.Fields.Item("U_DtAcq").Value = DateTime.Now;

                        FidoRighe.Add();

                }
            }
            */
        } 

    }
}
