//****************************************************************************
//
//  File:      B1Event.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;

namespace B1Base
{
  /// <summary>
  /// Base class for the generic events management on a event entity.
  /// </summary>
  /// <remarks>
  /// In your AddOn autogenerated project you will have one generic class inheriting 
  /// from B1Event that can handle one/several events either for a specified 
  /// list of form types or for all form types
  /// <para>The name of the inherited class will be EventsHandler </para>
  /// <para>The inherited class will contain a basic constructor plus one function per each
  /// listener added for the event.</para>
  /// </remarks>
  /// <example>
  /// This example shows the autogenerated class representing an event of type et_CLICK.
  /// <para>This class has the autogenerated code of a listeners receiving the ItemEvent et_CLICK 
  /// and consists of two methods. In the first case, the before event is handled only for a list of 
  /// form types (indicated by the new string[] {"150", "134"}). In the second case, the after event 
  /// is handled for all form types (indicated by the lack of new string[] parameter)</para>
  /// <code lang="Visual Basic" escaped="true">
  /// Public Class EventsHandler
  ///   Inherits B1Event
  /// 
  ///   Public Sub New()
  ///     MyBase.New
  ///   End Sub
  ///       
  ///   B1Listener(BoEventTypes.et_CLICK, True, New String() {"150", "134"}) _
  ///   Public Overridable Function OnBeforeClick(ByVal pVal As ItemEvent) As Boolean
  ///     Dim form As Form = B1Connections.theAppl.Forms.Item(pVal.FormUID)
  ///     'ADD YOUR ACTION CODE HERE ...
  ///     Return True
  ///   End Function
  ///
  ///   B1Listener(BoEventTypes.et_CLICK, False) _
  ///   Public Overridable Sub OnAfterClick(ByVal pVal As ItemEvent))
  ///     Dim ActionSuccess As Boolean = pVal.ActionSuccess
  ///     Dim form As Form = B1Connections.theAppl.Forms.Item(pVal.FormUID)
  ///     'ADD YOUR ACTION CODE HERE ...
  ///   End Sub
  /// End Class
  /// </code>
  /// <code lang="C#" escaped="true">
  /// public class EventsHandler : B1Event {
  ///      
  ///   public EventsHandler()
  ///   {
  ///   }
  ///
  ///   [B1Listener(BoEventTypes.et_CLICK, true, new string[] { "150", "134" })]
  ///   public virtual bool OnBeforeClick(ItemEvent pVal)
  ///   {
  ///     Form form = B1Connections.theAppl.Forms.Item(pVal.FormUID);
  ///     // ADD YOUR ACTION CODE HERE ...
  ///     return true;
  ///   }
  /// 
  ///   [B1Listener(BoEventTypes.et_CLICK, false)]
  ///   public virtual bool OnAfterClick(ItemEvent pVal) 
  ///   {
  ///     bool ActionSuccess = pVal.ActionSuccess;
  ///     Form form = B1Connections.theAppl.Forms.Item(pVal.FormUID)
  ///     // ADD YOUR ACTION CODE HERE ...
  ///   }
  /// }
  /// </code>
  /// </example>
  public abstract class B1Event : B1Action
  {
    /// <summary>
    /// Empty Constructor.
    /// </summary>
    protected B1Event()
    {
    }

    /// <summary>
    /// Returns the key identifying the generic event entity for the events management.
    /// </summary>
    /// <param name="before">Boolean value specifying whether the action
    /// wants to handle the before or the after notification.</param>
    /// <returns>String identifying the action key.</returns>
    public override sealed string GetKey(bool before)
    {
      return EventTables.GetActionKey(before);
    }
  }
}

