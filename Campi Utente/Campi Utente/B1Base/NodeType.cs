//****************************************************************************
//
//  File:      NodeType.cs
//
//  Copyright (c) SAP 
//
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
//****************************************************************************
using System;

namespace B1Base
{
  /// <summary>
  /// &quot;This class supports the B1Wizard and the B1UDOFormGenerator and
  /// is not intended to be used directly from your code.&quot;
  /// </summary>
  /// <remarks>This class supports the B1Wizard and the B1UDOFormGenerator and
  /// is not intended to be used directly from your code.</remarks>
  public class NodeType
  {
    public string typeName;
    public string varName;
    public string type;
  }
}
