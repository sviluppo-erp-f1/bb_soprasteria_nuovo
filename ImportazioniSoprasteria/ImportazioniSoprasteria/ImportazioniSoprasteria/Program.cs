﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Globalization;
using ImportazioniSoprasteria;


namespace ImportSoprasteria
{
    class Program
    {
        #region Dichiarazioni 
        private SAPbobsCOM.Company oCompany;
        private XmlDocument docXml = new XmlDocument();
        private SAPbobsCOM.Recordset oRecordImport;
        private SAPbobsCOM.Recordset oRecordSet;
        private SAPbobsCOM.Recordset oRecordArt;
        private SAPbobsCOM.Recordset oRecordDiBa;
        private SAPbobsCOM.Recordset oRecordFasi;
        private SAPbobsCOM.Recordset oRecordOrd;
        private SAPbobsCOM.Recordset oRecordDDT;
        private SAPbobsCOM.Recordset oRecord;
        private SAPbobsCOM.Recordset oRecordErr;
        private SAPbobsCOM.Items oItem;
        private SAPbobsCOM.IProductTrees oDist;
        private SAPbobsCOM.Documents oORDR;
        private SAPbobsCOM.Documents oDDT;
        private SAPbobsCOM.AlternateCatNum oOSCN;
        private SAPbobsCOM.CompanyService oService;
        private SAPbobsCOM.ProjectsService oPrjService;
        private SAPbobsCOM.Project oOPRJ;
        #endregion


        public static void Main(string[] args)
        {
            Program Prg = new Program();
            Prg.docXml.Load("parametriDB.xml");
            if (!SapParams.ConnessioneSAP(Prg.docXml, ref Prg.oCompany)) return;
            if (!SapParams.GetParams(Prg.docXml, ref Prg.oCompany)) return;

            Log.LogGenerale("Inizio importazioni del " + DateTime.Now);

            if (Prg.ElaboraImport())
            {
                Log.LogGenerale("Importazioni Terminate!");
            }
            else
            {
                Log.LogGenerale("Importazioni Terminate con ERRORI!");
            }
            Log.LogGenerale("");
            Log.LogGenerale("");
        }

        public bool ElaboraImport()
        {
            oRecordImport = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            if (SapParams.Values["ctrl_anno_commesse"] == "Y")
            {
                SapParams.Values["anni_commesse"] = SapParams.Values["anno_commesse"];
            }
            else
            {
                IEnumerable<int> squares = Enumerable.Range(2018, 60);
                string Anni = "";
                foreach (int num in squares)
                {
                    Anni += num.ToString() + ",";
                }
                SapParams.Values["anni_commesse"] = Anni.Substring(0, Anni.Length - 1);
            }

            //Ciclo sulle commesse per elaborarne una alla volta

            oRecordImport.DoQuery(Query.GetCommessa());
            if (oRecordImport.RecordCount == 0) Log.LogGenerale("Nessuna Commessa trovata!");
            while (!oRecordImport.EoF)
            {
                string CommessaID = oRecordImport.Fields.Item(0).Value.ToString();

                //Elaboro la commessa, se va bene committo, altrimenti nulla
                Log.LogGenerale("Elaborazione commessa : " + CommessaID);
                oCompany.StartTransaction();
                if (ElaboraCommessa(CommessaID)) oCompany.EndTransaction(SAPbobsCOM.BoWfTransOpt.wf_Commit);
                Log.LogGenerale("");
                //Pulisco la variabile da usare nei log
                SapParams.Values["commessaela"] = "";
                oRecordImport.MoveNext();
            }

            //INSERIMENTO DDT -> Non la gestisco tramite transazione ma diretta, visto che sono documenti indipendenti dal resto
            bool ResDDT = InsertDDT();
            return true;
        }

        public bool ElaboraCommessa(string CommessaID)
        {
            //Popolo una variabile globale per usarla nei vari log
            SapParams.Values["commessaela"] = CommessaID;
            /*
                    QUI INSERISCO TUTTI GLI OGGETTI DELLA COMMESSA DAI COMPONENTI ALL'ORDINE UNO ALLA VOLTA, CONTROLLANDO SE VA BENE O NO.
                    SE VA BENE PASSO AL METODO SUCCESSIVO
                    ALTRIMENTI ESCO E NON COMMITTO L'OPERAZIONE
            */

            //INSERIMENTO COMPONENTI    
            if (!InsertComponenti()) return false;

            //INSERIMENTO FINITI  
            if (!InsertFiniti()) return false;

            //INSERIMENTO FASI ORDINI  
            if (!InsertFasi()) return false;

            //INSERIMENTO DIBA
            if (!CiclaDiba(CommessaID)) return false;

            //INSERIMENTO ORDINE CLIENTE
            if (!InsertOrdine(CommessaID)) return false;

            return true;
        }

        public bool InsertComponenti()
        {
            //Dichiarazioni variabili metodo
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string ItemPrecedente = "";
            oRecordArt.DoQuery(Query.GetComponenti());

            //Ciclo sulla query
            while (!oRecordArt.EoF)
            {
                string ItemCode = oRecordArt.Fields.Item("ItemCode").Value.ToString();

                //Dalla Query estraggo gli articoli in ordine di aggiornamento desc, quindi prendo solo il primo e gli altri li salto
                if (ItemCode != ItemPrecedente)
                {
                    //Inserisco i dati dell'articolo
                    oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);

                    oItem.ItemCode = ItemCode;
                    oItem.ItemName = oRecordArt.Fields.Item("ItemName").Value;
                    oItem.BarCode = ItemCode;
                    oItem.ItemType = SAPbobsCOM.ItemTypeEnum.itItems;
                    oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                    oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                    oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tNO;
                    oItem.IntrastatExtension.IntrastatRelevant = SAPbobsCOM.BoYesNoEnum.tYES;
                    oItem.PlanningSystem = SAPbobsCOM.BoPlanningSystem.bop_MRP;
                    oItem.ProcurementMethod = SAPbobsCOM.BoProcurementMethod.bom_Make;
                    string UdM = oRecordArt.Fields.Item("U_FO_UNITID").Value;
                    oItem.SalesUnit = UdM;
                    oItem.PurchaseUnit = UdM;
                    oItem.InventoryUOM = UdM;
                    oItem.IntrastatExtension.IntrastatRelevant = SAPbobsCOM.BoYesNoEnum.tYES;
                    //RECUPERO GRUPPO ARTICOLI
                    string ProdClassID = oRecordArt.Fields.Item("U_FO_PRODUCTCALSSID").Value.ToString();
                    oRecord.DoQuery("select \"ItmsGrpCod\" from \"OITB\" where \"ItmsGrpNam\" = '" + ProdClassID + "'");
                    if (!oRecord.EoF)
                    {
                        oItem.ItemsGroupCode = int.Parse(oRecord.Fields.Item("ItmsGrpCod").Value.ToString());
                    }
                    else
                    {
                        oItem.ItemsGroupCode = int.Parse(SapParams.Values["grp_art_dft"]);
                    }

                    //RECUPERO IL CLIENTE E FORNITORE PREFERITO
                    string Company = oRecordArt.Fields.Item("U_FO_COMPANY").Value.ToString();
                    oRecord.DoQuery("SELECT \"U_FO_CARDCODE\" FROM \"@FO_CLICOMMESSE\" WHERE \"U_FO_COMPANY\" = '" + Company + "' AND \"U_TYPE\" = 'S' ");
                    oItem.Mainsupplier = oRecord.Fields.Item("U_FO_CARDCODE").Value.ToString();

                    //CAMPI UTENTE
                    oItem.UserFields.Fields.Item("U_FO_PRODUCTGROUPID").Value = oRecordArt.Fields.Item("U_FO_PRODUCTGROUPID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_LASTUPDATE").Value = oRecordArt.Fields.Item("U_FO_LASTUPDATE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_INVENTCOLORID").Value = oRecordArt.Fields.Item("Colore").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_FORNITORE").Value = oRecordArt.Fields.Item("U_FO_FORNITORE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_TIPOLOGIADIFFICOLTA").Value = oRecordArt.Fields.Item("U_FO_TIPOLOGIADIFFICOLTA").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_FLAGETICHETTASERIALE").Value = oRecordArt.Fields.Item("U_FO_FLAGETICHETTASERIALE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_LINEASTILE").Value = oRecordArt.Fields.Item("U_FO_LINEASTILE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_DESCRIZIONELINEASTILE").Value = oRecordArt.Fields.Item("U_FO_DESCRIZIONELINEASTILE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_COLORE").Value = oRecordArt.Fields.Item("COLORDESC").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_ITEMMODEL").Value = oRecordArt.Fields.Item("ItemID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_INVENTSIZEID").Value = oRecordArt.Fields.Item("U_FO_INVENTSIZEID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_INVENTCOLORID").Value = oRecordArt.Fields.Item("U_FO_INVENTCOLORID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_INVENTDROPID").Value = oRecordArt.Fields.Item("U_FO_INVENTDROPID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_SIZEROW").Value = oRecordArt.Fields.Item("U_FO_SIZEROW").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_INVENTSEASONID").Value = oRecordArt.Fields.Item("U_FO_INVENTSEASONID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_TRANSCTIONID").Value = oRecordArt.Fields.Item("U_FO_TRANSACTIONID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_CODCLIENTE").Value = oRecordArt.Fields.Item("U_FO_CARDCODE").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_MISURA").Value = oRecordArt.Fields.Item("U_FO_INVENTSIZEID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_U_FO_CODCOLORE").Value = oRecordArt.Fields.Item("U_FO_INVENTCOLORID").Value.ToString();
                    oItem.UserFields.Fields.Item("U_FO_CAD").Value = oRecordArt.Fields.Item("U_FO_CAD").Value.ToString();

                    //CAMPI UTENTE CON TAB UTENTE 
                    string Valore = "";
                    Valore = oRecordArt.Fields.Item("U_FO_COMPANY").Value.ToString();
                    if (Valore.Trim() != "")
                    {
                        oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_COMPANY\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                        if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_COMPANY\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                        oItem.UserFields.Fields.Item("U_FO_COMPANY").Value = Valore;
                    }
                    

                    Valore = oRecordArt.Fields.Item("U_FO_BRANDID").Value.ToString();
                    if (Valore.Trim() != "")
                    {
                        oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_BRANDID\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                        if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_BRANDID\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                        oItem.UserFields.Fields.Item("U_FO_BRANDID").Value = Valore;
                    }

                    Valore = oRecordArt.Fields.Item("U_FO_ITEMTYPE").Value.ToString();
                    if (Valore.Trim() != "")
                    {
                        oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_ITEMTYPE\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                        if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_ITEMTYPE\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                        oItem.UserFields.Fields.Item("U_FO_ITEMTYPE").Value = Valore;
                    }

                    //Se va bene vado avanti altrimenti gestisco l'errore tramite il metodo
                    int res = oItem.Add();
                    if (res != 0)
                    {
                        SapParams.GestisciErrore(ref oCompany, 1, ItemCode);
                        return false;
                    }
                    else
                    {
                        Log.LogGenerale("Articolo Componente: " + ItemCode + " creato correttamente");
                    }
                }
                //Ripopolo l'itemcode precedente con l'itemcode che ho appena ggiunto
                ItemPrecedente = ItemCode;
                oRecordArt.MoveNext();
            }
            return true;
        }

        public bool InsertFiniti()
        {
            //Variabili metodo
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt.DoQuery(Query.GetFiniti());

            //Ciclo sulal query
            while (!oRecordArt.EoF)
            {
                string ItemCode = oRecordArt.Fields.Item("ItemCode").Value.ToString();
                string CardCode = oRecordArt.Fields.Item("CardCode").Value.ToString();
                string SKU = oRecordArt.Fields.Item("ITEMID").Value.ToString();

                //Inizio a popolare i campi dell'articol finito
                oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                oItem.ItemCode = ItemCode;
                oItem.ItemName = oRecordArt.Fields.Item("ItemName").Value.ToString();
                oItem.BarCode = ItemCode;
                oItem.ItemType = SAPbobsCOM.ItemTypeEnum.itItems;
                oItem.InventoryItem = SAPbobsCOM.BoYesNoEnum.tYES;
                oItem.PurchaseItem = SAPbobsCOM.BoYesNoEnum.tNO;
                oItem.SalesItem = SAPbobsCOM.BoYesNoEnum.tYES;
                oItem.IntrastatExtension.IntrastatRelevant = SAPbobsCOM.BoYesNoEnum.tYES;
                string unit = oRecordArt.Fields.Item("UNITID").Value;
                oItem.SalesUnit = unit;
                oItem.PurchaseUnit = unit;
                oItem.InventoryUOM = unit;

                //RECUPERO GRUPPO ARTICOLI
                oRecord.DoQuery("select \"ItmsGrpCod\" from \"OITB\" where \"ItmsGrpCod\" = '" + SapParams.Values["grp_art_finiti"].ToString() + "'");
                if (!oRecord.EoF)
                {
                    oItem.ItemsGroupCode = int.Parse(SapParams.Values["grp_art_finiti"].ToString());
                }
                else
                {
                    oItem.ItemsGroupCode = int.Parse(SapParams.Values["grp_art_dft"]);
                }

                oRecord.DoQuery(Query.GetMagOrdine(oRecordArt.Fields.Item("COMPANY").Value.ToString()));
                    oItem.DefaultWarehouse = oRecord.Fields.Item(0).Value.ToString();

                //Popolo campi utente
                oItem.UserFields.Fields.Item("U_FO_ITEMMODEL").Value = oRecordArt.Fields.Item("ITEMMODEL").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_INVENTSIZEID").Value = oRecordArt.Fields.Item("INVENTSIZEID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_INVENTCOLORID").Value = oRecordArt.Fields.Item("INVENTCOLORID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_INVENTDROPID").Value = oRecordArt.Fields.Item("INVENTDROPID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_SIZEROW").Value = oRecordArt.Fields.Item("SIZEROW").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_INVENTSEASONID").Value = oRecordArt.Fields.Item("INVENTSEASONID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_COLORE").Value = oRecordArt.Fields.Item("COLORDESC").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_LASTUPDATE").Value = oRecordArt.Fields.Item("LASTUPDATE").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_TRANSCTIONID").Value = oRecordArt.Fields.Item("TRANSACTIONID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_CODCLIENTE").Value = oRecordArt.Fields.Item("CardCode").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_MISURA").Value = oRecordArt.Fields.Item("INVENTSIZEID").Value.ToString();
                oItem.UserFields.Fields.Item("U_U_FO_CODCOLORE").Value = oRecordArt.Fields.Item("INVENTCOLORID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_PRODUCTGROUPID").Value = oRecordArt.Fields.Item("PRODUCTGROUPID").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_TIPOLOGIADIFFICOLTA").Value = oRecordArt.Fields.Item("TIPOLOGIADIFFICOLTA").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_FLAGETICHETTASERIALE").Value = oRecordArt.Fields.Item("FLAGETICHETTASERIALE").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_LINEASTILE").Value = oRecordArt.Fields.Item("LINEASTILE").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_DESCRIZIONELINEASTILE").Value = oRecordArt.Fields.Item("DESCRIZIONELINEASTILE").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_CAD").Value = oRecordArt.Fields.Item("CAD").Value.ToString();
                oItem.UserFields.Fields.Item("U_FO_FORNITORE").Value = oRecordArt.Fields.Item("FORNITORE").Value.ToString();

                //CAMPI UTENTE CON TAB UTENTE 
                string Valore = "";
                Valore = oRecordArt.Fields.Item("COMPANY").Value.ToString();
                if (Valore.Trim() != "")
                {
                    oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_COMPANY\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                    if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_COMPANY\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                    oItem.UserFields.Fields.Item("U_FO_COMPANY").Value = Valore;
                }

                Valore = oRecordArt.Fields.Item("BRANDID").Value.ToString();
                if (Valore.Trim() != "")
                {
                    oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_BRANDID\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                    if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_BRANDID\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                    oItem.UserFields.Fields.Item("U_FO_BRANDID").Value = Valore;
                }

                Valore = oRecordArt.Fields.Item("ITEMTYPE").Value.ToString();
                if (Valore.Trim() != "")
                {
                    oRecord.DoQuery("SELECT \"Code\" FROM \"@FO_ITEMTYPE\" WHERE \"Code\" = '" + Valore + "' OR \"Name\" = '" + Valore + "'");
                    if (oRecord.RecordCount == 0) oRecord.DoQuery("INSERT INTO \"@FO_ITEMTYPE\" (\"Code\", \"Name\") VALUES ('" + Valore + "','" + Valore + "' )");
                    oItem.UserFields.Fields.Item("U_FO_ITEMTYPE").Value = Valore;
                }

                //Inserisco il finito e se va male gestisco l'errore
                int res = oItem.Add();
                if (res != 0)
                {
                    SapParams.GestisciErrore(ref oCompany, 2, ItemCode);
                    return false;
                }
                else
                {
                    Log.LogGenerale("Articolo Finito: " + ItemCode + " creato correttamente");
                    InsertNumCatBP(ItemCode, CardCode, SapParams.Values["commessaela"], SKU);
                }

                oRecordArt.MoveNext();
            }
            return true;
        }

        public bool InsertFasi()
        {
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt.DoQuery(Query.GetFasi());

            while (!oRecordArt.EoF)
            {
                oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);

                string ItemCode = oRecordArt.Fields.Item("ItemCode").Value.ToString();
                oItem.ItemCode = ItemCode;
                oItem.ItemName = oRecordArt.Fields.Item("ItemName").Value.ToString(); ;
                oItem.ItemType = SAPbobsCOM.ItemTypeEnum.itLabor;
                oItem.IntrastatExtension.IntrastatRelevant = SAPbobsCOM.BoYesNoEnum.tNO;

                int res = oItem.Add();
                if (res != 0)
                {
                    SapParams.GestisciErrore(ref oCompany, 3, ItemCode);
                    return false;
                }
                else
                {
                    Log.LogGenerale("Fase Commessa: " + ItemCode + " creata correttamente");
                }

                oRecordArt.MoveNext();
            }

            return true;
        }

        public bool CiclaDiba(string CommessaID)
        {
            oDist = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductTrees);
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordDiBa = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordDiBa.DoQuery(Query.GetDistinte());

            while (!oRecordDiBa.EoF)
            {
                bool TagliaUnica = false;
                string ThisDibaCode = oRecordDiBa.Fields.Item("DibaCode").Value.ToString();

                //VERIFICO SE TAGLIA CORRENTE O TAGLIA UNICA
                oRecord.DoQuery("SELECT \"ItemCode\" FROM OITM WHERE \"ItemCode\" = '" + ThisDibaCode + "'");
                if (oRecord.RecordCount == 0) TagliaUnica = true;

                //SE TAGLIA PASSATA DA DIBA E' CORRETTA INSERISCO LA DIBA ALTRIMENTI LA INSERISCO COME TAGLIA UNICA
                if (!TagliaUnica)
                {
                    //Controllo che nel frattempo non è già stata inserita
                    string QueryControllo = "SELECT \"Code\" FROM \"OITT\" WHERE \"Code\" = '" + ThisDibaCode + "'";
                    oRecord.DoQuery(QueryControllo);
                    if (oRecord.RecordCount == 0) if (!InsertDiba(ThisDibaCode, ThisDibaCode)) return false;

                }
                else
                {
                    //Recupero gli articoli con quel ItemId e variante colore e ci creo una Distinta Base
                    string ItemID = oRecordDiBa.Fields.Item("ItemID").Value.ToString();
                    string ColorID = oRecordDiBa.Fields.Item("ColorID").Value.ToString();

                    string QueryDi = "";
                    QueryDi += " SELECT T0.\"ItemCode\" FROM OITM T0 WHERE T0.\"ItemCode\" Like '" + ItemID + "_%%' AND T0.\"ItemCode\" Like '%%_" + ColorID + "' ";
                    QueryDi += "     AND NOT EXISTS(       ";
                    QueryDi += "           SELECT TA.\"Code\" FROM OITT TA       ";
                    QueryDi += "           WHERE TA.\"Code\" = T0.\"ItemCode\"       ";
                    QueryDi += "           )        ";
                    oRecord.DoQuery(QueryDi);
                    while (!oRecord.EoF)
                    {
                        string DibaCode = oRecord.Fields.Item("ItemCode").Value.ToString();
                        if (!InsertDiba(DibaCode, ThisDibaCode)) return false;
                        oRecord.MoveNext();
                    }
                }
                oRecordDiBa.MoveNext();
            }
            return true;
        }

        public bool InsertDiba(string DibaCode, string DibaQuery)
        {
            //Dichiarazioni Variabili per metodo
            oDist = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oProductTrees);
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordFasi = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            int CountFasi = 0;
            string FasePrecedente = "";

            //INIZIO CREAZIONE DISTINTA BASE
            oDist.TreeCode = DibaCode;
            oDist.UserFields.Fields.Item("U_FO_CONTROLLATA").Value = "NO";

            //RECUPERO LA DESCRIZIONE DELL'ARTICOLO PADRE
            oRecord.DoQuery("SELECT \"ItemName\" FROM OITM WHERE \"ItemCode\"='" + DibaCode + "' ");
            string ItemName = oRecord.Fields.Item(0).Value.ToString();
            oRecordFasi.DoQuery(Query.GetFasiDIBA(DibaQuery));

            //Ciclo sulle fasi 
            CountFasi = 1;
            int CountAllFasi = oRecordFasi.RecordCount;
            while (!oRecordFasi.EoF)
            {
                string NomeFile = oRecordFasi.Fields.Item("NomeFile").Value.ToString();
                string Fase = oRecordFasi.Fields.Item("Fase").Value.ToString();

                //Recupero ID Fase, se la trovo la inserisco in diba, altrimenti scrivo log errori
                oRecord.DoQuery("SELECT \"AbsEntry\" FROM ORST WHERE \"Code\" ='" + Fase + "'");
                oDist.Stages.SequenceNumber = CountFasi;
                if (oRecord.EoF)
                {
                    Log.LogBB("           Fase " + Fase + " Non trovata su sap");
                }
                else
                {
                    oDist.Stages.StageEntry = int.Parse(oRecord.Fields.Item(0).Value.ToString());
                }

                //Ciclo su gli articoli per quella fase
                oRecordArt.DoQuery(Query.GetArticoliDIBA(Fase, NomeFile, DibaQuery));

                //Inserimento Semilavorati
                //Controllo Se C'è gia una fase prima di questa e inseisco l'avanzamento del precedente semilavorato
                if (FasePrecedente != "")
                {
                    string ItemSemiLavScarico = DibaCode + "_" + FasePrecedente;
                    InsertSemilavorato(ItemSemiLavScarico, ItemName);
                    oDist.Items.ItemCode = ItemSemiLavScarico;
                    oDist.Items.ItemCode = ItemSemiLavScarico;
                    oDist.Items.Quantity = double.Parse("1");
                    oDist.Items.StageID = CountFasi;
                    oDist.Items.Warehouse = SapParams.Values["mag_art_ctrl"];
                    oDist.Items.IssueMethod = SAPbobsCOM.BoIssueMethod.im_Manual;
                    oDist.Items.Add();
                }


                while (!oRecordArt.EoF) //Ciclo su articoli
                {
                    string Componente = oRecordArt.Fields.Item("ItemCode").Value.ToString();
                    if (Componente != "")
                    {

                        //Recupero il Magazzino  in base a dei parametri fissi e l'articolo alternativo
                        string Magazzino = SapParams.Values["mag_default"].ToString();
                        string ComponenteSub = Componente.Substring(0, 6);

                        if (oRecordArt.Fields.Item("Prop").Value.ToString() == "2") //Se GOODSPROPERTY BB
                        {
                            string GetArtAlt = " SELECT \"AltItem\" FROM \"OALI\" WHERE \"OrigItem\" = '" + Componente + "'";
                            oRecord.DoQuery(GetArtAlt);
                            if (oRecord.RecordCount > 0) { Componente = oRecord.Fields.Item(0).Value.ToString(); }
                        }
                        string Prop = oRecordArt.Fields.Item("Prop").Value.ToString();
                        string Comp = oRecordArt.Fields.Item("Company").Value.ToString();
                        string MagQuery = Query.GetMagazzinoDIBATable(oRecordArt.Fields.Item("Prop").Value.ToString(),
                                                                        oRecordArt.Fields.Item("Company").Value.ToString(),
                                                                        ComponenteSub, Fase
                                                                    );
                        oRecord.DoQuery(MagQuery);
                        if (oRecord.Fields.Item(0).Value.ToString().Trim() != "") Magazzino = oRecord.Fields.Item(0).Value.ToString();
                        oRecord.DoQuery("SELECT \"WhsCode\" FROM \"OWHS\" WHERE \"WhsCode\" = '" + Magazzino + "' AND \"Inactive\" = 'N' ");
                        if (oRecord.RecordCount == 0)
                        {
                            Log.LogBB("Errore Inserimento Distinta Base : " + DibaCode + " Il magazzino " + Magazzino + " non esiste su sap o è disattivato! In distinta base è stato impostato il magazzino Mc");
                            Magazzino = SapParams.Values["mag_default"].ToString();
                        }

                        //Inserisco l'articolo 
                        oDist.Items.ItemCode = SapParams.ControllaArticolo(Componente, ref oCompany);
                        //oDist.Items.PriceList = 0;
                        oDist.Items.Quantity = double.Parse(oRecordArt.Fields.Item("Qty").Value.ToString());
                        oDist.Items.Warehouse = Magazzino;
                        oDist.Items.StageID = CountFasi;
                        oDist.Items.IssueMethod = SAPbobsCOM.BoIssueMethod.im_Manual;
                        oDist.Items.Add();
                    }
                    oRecordArt.MoveNext();
                }

                //Controllo se è ultima fase, nel caso creo il semilavorato
                if (CountFasi != CountAllFasi)
                {
                    string ItemSemiLavCarico = DibaCode + "_" + Fase;
                    InsertSemilavorato(ItemSemiLavCarico, ItemName);
                    oDist.Items.ItemCode = ItemSemiLavCarico;
                    oDist.Items.Quantity = double.Parse("-1");
                    oDist.Items.StageID = CountFasi;
                    oDist.Items.Warehouse = SapParams.Values["mag_art_ctrl"];
                    oDist.Items.IssueMethod = SAPbobsCOM.BoIssueMethod.im_Manual;
                    FasePrecedente = Fase;
                    oDist.Items.Add();
                }

                oDist.Stages.Add();
                CountFasi = CountFasi + 1;
                oRecordFasi.MoveNext();
            }

            //Inserisco e se va male gestisco l'errore con il metodo apposito
            int res = oDist.Add();
            if (res != 0)
            {
                SapParams.GestisciErrore(ref oCompany, 4, DibaQuery);
                return false;
            }
            else
            {
                Log.LogGenerale("Distinta Base: " + DibaCode + " creata correttamente");
            }
            return true;
        }

        public void InsertSemilavorato(string ItemCode, string ItemName)
        {
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            //Check Esistenza 
            oRecord.DoQuery("SELECT \"ItemName\" FROM \"OITM\" WHERE  \"ItemCode\" = '" + ItemCode + "'");
            if (oRecord.RecordCount > 0) { }
            else
            {
                oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
                oItem.ItemsGroupCode = int.Parse(SapParams.Values["grp_art_semi"]);
                oItem.ItemCode = ItemCode;
                oItem.ItemName = ItemName;

                int res = oItem.Add();
                if (res != 0)
                {
                    SapParams.GestisciErrore(ref oCompany, 8, ItemCode);
                }
                else
                {
                    Log.LogGenerale("Articolo Semilavorato: " + ItemCode + " creato correttamente");
                }
            }
        }

        public bool InsertOrdine(string CommessaID)
        {
            oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordOrd = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordFasi = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            string CommessaNoRow = CommessaID.Substring(0, CommessaID.Length - 2);

            oRecordOrd.DoQuery(Query.GetOrdine());
            oORDR = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oOrders);

            string NomeFile = oRecordOrd.Fields.Item("NomeFile").Value.ToString();
            string CompanyOrd = oRecordOrd.Fields.Item("COMPANY").Value;
            string CardCode = oRecordOrd.Fields.Item("CardC").Value;
            string NumAtCard = oRecordOrd.Fields.Item("Ordine").Value;
            oORDR.CardCode = CardCode;
            oORDR.NumAtCard = NumAtCard;
            oORDR.DocDueDate = oRecordOrd.Fields.Item("Date").Value;

            //Recupero SALESTYPE E INVENTLOCATIONGUCCI 
            string QueryCampi = " SELECT T0.\"U_FO_SALESTYPE\", T1.\"U_FO_INVENTLOCATIONGUCCI\" FROM \"@FO_COMMESSAHEADOUT\" T0 LEFT JOIN \"@FO_COMMESSABOMOUT\" T1  ";
            QueryCampi += " ON T0.\"U_FO_COMMESSAID\" = T1.\"U_FO_COMMESSAID\"  WHERE T0.\"U_FO_COMMESSAID\" = '" + CommessaNoRow + "'";
            oRecord.DoQuery(QueryCampi);
            if (oRecord.RecordCount > 0)
            {
                oORDR.UserFields.Fields.Item("U_FO_SALESTYPE").Value = oRecord.Fields.Item(0).Value.ToString();
                oORDR.UserFields.Fields.Item("U_FO_INVENTLOCATIONGUCCI").Value = oRecord.Fields.Item(1).Value.ToString();
            }

            //RECUPERO TIPO COMMESSA e Causale Trasporto
            string QueryCom = " SELECT T1.\"U_FO_TIPOSAP\", T1.\"U_FO_CAUSTRASP\" FROM \"@FO_COMMESSAHEADOUT\" T0 INNER JOIN \"@FO_TIPOCOMMESSA\" T1 ON T0.\"U_FO_COMMESSATYPE\" ";
            QueryCom += " =T1.\"U_FO_TIPOCOMM\" AND T0.\"U_FO_SALESTYPE\" = T1.\"U_FO_TIPOVEND\" WHERE T0.\"U_FO_COMMESSAID\"  = '" + CommessaNoRow + "' ";
            oRecord.DoQuery(QueryCom);
            if (oRecord.RecordCount > 0)
            {
                oORDR.UserFields.Fields.Item("U_FO_TIPOCOMMESSA").Value = oRecord.Fields.Item(0).Value.ToString();
                oORDR.UserFields.Fields.Item("U_TIPOCOMMESSA").Value = oRecord.Fields.Item(0).Value.ToString();
                oORDR.UserFields.Fields.Item("U_CAUSALETRASPORTO").Value = oRecord.Fields.Item(1).Value.ToString();
            }
            else
            {
                oORDR.UserFields.Fields.Item("U_FO_TIPOCOMMESSA").Value = "ALTRO";
                oORDR.UserFields.Fields.Item("U_TIPOCOMMESSA").Value = "ALTRO";
                Log.LogBB("Errore Inserimento Ordine Cliente : " + CommessaID + ", Tipo Commessa SAP Mancante ");
            }

            //RECUPERO STAGIONE COMMESSA
            string QueryStag = " SELECT T1.\"Code\" FROM \"@FO_COMMESSAHEADOUT\" T0 INNER JOIN \"@F1_STAGIONI\" T1 ON T0.\"U_FO_INVENTSEASONID\"=T1.\"Code\" ";
            QueryStag += "  WHERE T0.\"U_FO_COMMESSAID\" = '" + CommessaNoRow + "' ";
            string StagioneORDR = "";
            oRecord.DoQuery(QueryStag);
            if (oRecord.RecordCount > 0)
            {
                StagioneORDR = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                //Inserisco la stagione in tab utente e eseguo nuovamente la query
                string GetStag = " SELECT T0.\"U_FO_INVENTSEASONID\" FROM \"@FO_COMMESSAHEADOUT\" T0  WHERE T0.\"U_FO_COMMESSAID\" = '" + CommessaNoRow + "'  ";
                oRecord.DoQuery(GetStag);
                if (oRecord.RecordCount > 0)
                {
                    string StagioneORIG = oRecord.Fields.Item(0).Value.ToString();
                    string InsStag = " INSERT INTO \"@F1_STAGIONI\" ( \"Code\", \"Name\") VALUES ( '" + StagioneORIG + "', '" + StagioneORIG + "')";
                    oRecord.DoQuery(InsStag);

                    oRecord.DoQuery(QueryStag);
                    if (oRecord.RecordCount > 0)
                    {
                        StagioneORDR = oRecord.Fields.Item(0).Value.ToString();
                    }
                }
            }

            if (StagioneORDR == "")
            {
                oORDR.UserFields.Fields.Item("U_STAGIONE").Value = "ALTRO";
                Log.LogBB("Errore Inserimento Ordine Cliente : " + CommessaID + ", Stagione Commessa SAP Mancante ");
            }
            else
            {
                oORDR.UserFields.Fields.Item("U_STAGIONE").Value = StagioneORDR;
            }


            //RECUPERO PROGETTO SE NON ESISTE LO CREO
            string Progetto = "";
            oRecord.DoQuery("SELECT \"PrjCode\" FROM OPRJ WHERE \"PrjCode\" = '" + oRecordOrd.Fields.Item("MASTERORDERID").Value + "'");
            if (oRecord.RecordCount > 0)
            {
                Progetto = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                InsertProgetto(oRecordOrd.Fields.Item("MASTERORDERID").Value.ToString());
                //Riprovo e verifico che lo abbia creato
                oRecord.DoQuery("SELECT \"PrjCode\" FROM OPRJ WHERE \"PrjCode\" = '" + oRecordOrd.Fields.Item("MASTERORDERID").Value + "'");
                if (oRecord.RecordCount > 0)
                {
                    Progetto = oRecord.Fields.Item(0).Value.ToString();
                }
                else
                {
                    Log.LogBB("Errore Inserimento Ordine Cliente : " + CommessaID + ", Errore Creazione Progetto: " + oRecordOrd.Fields.Item("MASTERORDERID").Value.ToString() + ", crearlo manualmente");
                    return false;
                }
            }

            oORDR.Project = Progetto;

            //Ciclo gli articoli
            int Counter = 0;
            int ArtCount = 0;
            oRecordArt.DoQuery(Query.GetArticoliOrdine(NomeFile));
            while (!oRecordArt.EoF)
            {
                Counter++;
                ArtCount++;

                string ItemCode = oRecordArt.Fields.Item("ItemCode").Value.ToString();
                double Qty = Double.Parse(oRecordArt.Fields.Item("Qty").Value.ToString());

                oORDR.Lines.ItemCode = SapParams.ControllaArticolo(ItemCode, ref oCompany);
                oORDR.Lines.Quantity = Qty;
                oORDR.Lines.ProjectCode = Progetto;
                oORDR.Lines.UserFields.Fields.Item("U_v").Value = Counter.ToString();

                //Recupero il magazzino 
                oRecord.DoQuery(Query.GetMagOrdine(CompanyOrd));
                if (oRecord.Fields.Item(0).Value.ToString() != "")
                    oORDR.Lines.WarehouseCode = oRecord.Fields.Item(0).Value.ToString();
                
                oORDR.Lines.Add();

                //Ciclo sulle fasi per l'articolo 

                oRecordFasi.DoQuery(Query.GetFasiOrdine(NomeFile, ItemCode));
                while (!oRecordFasi.EoF)
                {
                    Counter++;
                    oORDR.Lines.ItemCode = oRecordFasi.Fields.Item("Fase").Value.ToString();
                    oORDR.Lines.Quantity = Qty;
                    oORDR.Lines.ProjectCode = Progetto;
                    oORDR.Lines.UnitPrice = Double.Parse(oRecordFasi.Fields.Item("UNITPRZ").Value.ToString());
                    oORDR.Lines.UserFields.Fields.Item("U_FO_LNCNT").Value = Counter.ToString();
                    oORDR.Lines.UserFields.Fields.Item("U_FO_LNREF").Value = ArtCount.ToString();
                    oORDR.Lines.Add();
                    oRecordFasi.MoveNext();
                }
                oRecordArt.MoveNext();
            }

            int res = oORDR.Add();
            if (res != 0)
            {
                SapParams.GestisciErrore(ref oCompany, 5, NumAtCard);
                return false;
            }
            else
            {
                Log.LogGenerale("Ordine: " + NumAtCard + " creato correttamente");
            }

            return true;
        }

        public bool InsertNumCatBP(string ItemCode, string CardCode, string CommessaID, string SKU)
        {
            oOSCN = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oAlternateCatNum);
            oOSCN.ItemCode = ItemCode;
            oOSCN.CardCode = CardCode;
            oOSCN.Substitute = SKU;

            int res = oOSCN.Add();
            if (res != 0)
            {
                SapParams.GestisciErrore(ref oCompany, 6, CardCode + " - " + ItemCode);
                return false;
            }
            else
            {
                Log.LogGenerale("Numero di Catalogo per BP: " + CardCode + " e Articolo: " + ItemCode + " creato correttamente");
            }
            return true;
        }

        public void InsertProgetto(string PrjCode)
        {
            oService = oCompany.GetCompanyService();
            oPrjService = oService.GetBusinessService(SAPbobsCOM.ServiceTypes.ProjectsService);
            oOPRJ = oPrjService.GetDataInterface(SAPbobsCOM.ProjectsServiceDataInterfaces.psProject);

            oOPRJ.Code = PrjCode;
            oOPRJ.Name = PrjCode;
            oOPRJ.ValidFrom = DateTime.Now;

            oPrjService.AddProject(oOPRJ);
        }

        public bool InsertDDT()
        {
            oItem = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oItems);
            oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordDDT = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordArt = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecordFasi = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);

            oRecordDDT.DoQuery(Query.GetDDT());
            while (!oRecordDDT.EoF)
            {
                oDDT = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oDrafts);
                oDDT.DocObjectCode = SAPbobsCOM.BoObjectTypes.oPurchaseDeliveryNotes;

                string NomeFile = oRecordDDT.Fields.Item("NomeFile").Value.ToString();
                string CodiceDDT = oRecordDDT.Fields.Item("NumAtCard").Value.ToString();

                oDDT.CardCode = oRecordDDT.Fields.Item("CardCode").Value.ToString();
                oDDT.DocDate = Convert.ToDateTime(oRecordDDT.Fields.Item("DocDate").Value.ToString());
                oDDT.NumAtCard = CodiceDDT;

                oRecordArt.DoQuery(Query.GetArticoliDDT(CodiceDDT, NomeFile));

                while (!oRecordArt.EoF)
                {

                    oDDT.Lines.ItemCode = SapParams.ControllaArticolo(oRecordArt.Fields.Item("ItemCode").Value.ToString(), ref oCompany);
                    oDDT.Lines.Quantity = Double.Parse(oRecordArt.Fields.Item("Qty").Value.ToString());
                    oDDT.Lines.Add();
                    oRecordArt.MoveNext();
                }

                int res = oDDT.Add();
                if (res != 0)
                {
                    SapParams.GestisciErrore(ref oCompany, 7, CodiceDDT);
                }
                else
                {
                    Log.LogGenerale("Entrata Merci: " + CodiceDDT + " creata correttamente");
                }
                Log.LogGenerale("");
                oRecordDDT.MoveNext();
            }
            return true;
        }
    }
}
