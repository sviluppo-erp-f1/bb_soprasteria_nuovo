﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportazioniSoprasteria
{
    public class Query
    {
        public static string GetCommessa()
        {
            string GetCommessa = "SELECT DISTINCT CONCAT(T0.\"U_FO_COMMESSAID\",CONCAT('_', \"U_FO_COMMESSAROWID\")) from \"@FO_COMMESSABOMOUT\" T0  INNER JOIN ";
            GetCommessa += " \"@FO_COMMESSAHEADOUT\" T2 ON T2.\"U_FO_COMMESSAID\" = T0.\"U_FO_COMMESSAID\" INNER JOIN  \"@FO_COMMESSAPOSOUT\" T3  ON T3.\"U_FO_COMMESSA\" = T0.\"U_FO_COMMESSAID\"  WHERE NOT EXISTS ";
            GetCommessa += "      (SELECT T1.\"NumAtCard\" FROM ORDR T1 WHERE T1.\"NumAtCard\" = CONCAT(T0.\"U_FO_COMMESSAID\",CONCAT('_', \"U_FO_COMMESSAROWID\")))";
            GetCommessa += "      AND YEAR(TO_DATE(SUBSTRING(T3.\"U_FO_DLVDATE\", 0, 10), 'DD/MM/YYYY')) IN (" + SapParams.Values["anni_commesse"] + ") ";
            return GetCommessa;
        }

        public static string GetComponenti()
        {
            string QueryComponenti = "" ;
            //Query per estrarre i componenti per quella commessa
            QueryComponenti += "SELECT DISTINCT *  FROM (            ";
            QueryComponenti += "    SELECT T1.\"U_FO_ITEMID\" as \"ItemID\",             ";
            QueryComponenti += "        T0.\"U_FO_ITEMID\"||'_0'||IFNULL(T0.\"U_FO_INVENTSIZEID\", '000')||'_'||IFNULL(T0.\"U_FO_INVENTCOLORID\", '0000') as \"ItemCode\",             ";
            QueryComponenti += "        T0.\"U_FO_INVENTSIZEID\" ,   ";
            QueryComponenti += "        T0.\"U_FO_INVENTCOLORID\" ,   "; 
            QueryComponenti += "        T0.\"U_FO_INVENTDROPID\" ,   ";
            QueryComponenti += "        T1.\"U_FO_BRANDID\" ,   ";
            QueryComponenti += "        T1.\"U_FO_PRODUCTCALSSID\" ,             ";
            QueryComponenti += "        T1.\"U_FO_ITEMNAME\"||'    '||IFNULL(T2.\"U_FO_DIMENSIONNAME\",  ' ') AS \"ItemName\" ,             ";
            QueryComponenti += "        T1.\"U_FO_UNITID\",             ";
            QueryComponenti += "        T1.\"U_FO_ITEMTYPE\" ,             ";
            QueryComponenti += "        T1.\"U_FO_PRODUCTGROUPID\" ,             ";
            QueryComponenti += "        T1.\"U_FO_COMPANY\",             ";
            QueryComponenti += "        T1.\"U_FO_FORNITORE\" ,             ";
            QueryComponenti += "        T1.\"U_FO_TIPOLOGIADIFFICOLTA\",             ";
            QueryComponenti += "        T1.\"U_FO_FLAGETICHETTASERIALE\" ,             ";
            QueryComponenti += "        T1.\"U_FO_LINEASTILE\" ,             ";
            QueryComponenti += "        T1.\"U_FO_DESCRIZIONELINEASTILE\",             ";
            QueryComponenti += "        T1.\"U_FO_LASTUPDATE\" ,             ";
            QueryComponenti += "        T0.\"U_FO_INVENTCOLORID\" as \"Colore\",             ";
            QueryComponenti += "        T0.\"U_FO_INVENTSIZEID\" as \"Taglia\",             ";
            QueryComponenti += "        T0.\"U_FO_INVENTDROPID\" as \"Crop\",             ";
            QueryComponenti += "        IFNULL(T2.\"U_FO_DIMENSIONNAME\", ' ') as \"COLORDESC\",             ";
            QueryComponenti += "        T0.\"U_FO_SIZEROW\",             ";
            QueryComponenti += "        T4.\"U_FO_CARDCODE\",              ";
            QueryComponenti += "        T0.\"U_FO_TRANSACTIONID\",              ";
            QueryComponenti += "        NULL AS \"U_FO_INVENTSEASONID\",              ";
            QueryComponenti += "        T1.\"U_FO_CAD\"              ";

            QueryComponenti += "    FROM              ";
            QueryComponenti += "       \"@FO_COMMESSABOMOUT\" T0             ";
            QueryComponenti += "        INNER JOIN \"@FO_MASTERITEM\" T1 ON T0.\"U_FO_ITEMID\" = T1.\"U_FO_ITEMID\"             ";
            QueryComponenti += "        LEFT JOIN \"@FO_MASTERVARIANT\" T2 ON T0.\"U_FO_ITEMID\" = T2.\"U_FO_ITEMID\" AND T0.\"U_FO_INVENTCOLORID\" = T2.\"U_FO_DIMENSIONID\"             ";
            QueryComponenti += "            AND T2.\"U_FO_DIMENSIONTYPE\" = '1'             ";
            QueryComponenti += "        INNER JOIN \"@FO_CLICOMMESSE\" T4 ON T0.\"U_FO_COMPANY\" = T4.\"U_FO_COMPANY\"  AND T4.\"U_TYPE\" = 'C'             ";
            QueryComponenti += "    WHERE              ";
            QueryComponenti += "        CONCAT(T0.\"U_FO_COMMESSAID\",CONCAT('_', \"U_FO_COMMESSAROWID\")) = '" + SapParams.Values["commessaela"] + "'              ";
            QueryComponenti += "        AND NOT EXISTS (             ";
            QueryComponenti += "                    SELECT TA.\"ItemCode\" FROM OITM TA              ";
            QueryComponenti += "                    WHERE TA.\"ItemCode\" = T0.\"U_FO_ITEMID\"||'_0'||IFNULL(T0.\"U_FO_INVENTSIZEID\", '000')||'_'||IFNULL(T0.\"U_FO_INVENTCOLORID\", '0000')             ";
            QueryComponenti += "                )             ";
            QueryComponenti += "UNION ALL             ";
            QueryComponenti += "       SELECT             ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTID\" as \"ItemID\",                          ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTID\"||'_0'||IFNULL(T0.\"U_FO_COMPONENTSIZEID\", '000')||'_'||IFNULL(T0.\"U_FO_COMPONENTCOLORID\", '0000') as \"ItemCode\",                    ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTSIZEID\" AS \"U_FO_INVENTSIZEID\" ,   ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTCOLORID\" AS \"U_FO_INVENTCOLORID\" ,   ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTDROPID\" AS \"U_FO_INVENTDROPID\" ,   ";
            QueryComponenti += "           T1.\"U_FO_BRANDID\" ,                ";
            QueryComponenti += "           T1.\"U_FO_PRODUCTCALSSID\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_ITEMNAME\"||'    '||IFNULL(T2.\"U_FO_DIMENSIONNAME\",  ' ') AS \"ItemName\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_UNITID\",                          ";
            QueryComponenti += "           T1.\"U_FO_ITEMTYPE\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_PRODUCTGROUPID\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_COMPANY\",                          ";
            QueryComponenti += "           T1.\"U_FO_FORNITORE\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_TIPOLOGIADIFFICOLTA\",                          ";
            QueryComponenti += "           T1.\"U_FO_FLAGETICHETTASERIALE\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_LINEASTILE\" ,                          ";
            QueryComponenti += "           T1.\"U_FO_DESCRIZIONELINEASTILE\",                          ";
            QueryComponenti += "           T1.\"U_FO_LASTUPDATE\" ,                          ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTCOLORID\" as \"Colore\",                          ";
            QueryComponenti += "           T0.\"U_FO_COMPONENTSIZEID\" as \"Taglia\",                          ";
            QueryComponenti += "           NULL as \"Crop\",                          ";
            QueryComponenti += "           IFNULL(T2.\"U_FO_DIMENSIONNAME\", ' ') as \"COLORDESC\" ,                         ";
            QueryComponenti += "           T3.\"U_FO_SIZEROW\",             ";
            QueryComponenti += "           T4.\"U_FO_CARDCODE\",              ";
            QueryComponenti += "           T3.\"U_FO_TRANSACTIONID\",              ";
            QueryComponenti += "           T0.\"U_FO_INVENTSEASONID\",              ";
            QueryComponenti += "           T1.\"U_FO_CAD\"              ";
            QueryComponenti += "       FROM                           ";
            QueryComponenti += "   	    \"@FO_MASTERMATERIAL\" T0                          ";
            QueryComponenti += "           INNER JOIN \"@FO_MASTERITEM\" T1 ON T0.\"U_FO_COMPONENTID\" = T1.\"U_FO_ITEMID\"                          ";
            QueryComponenti += "           LEFT JOIN \"@FO_MASTERVARIANT\" T2 ON T0.\"U_FO_COMPONENTID\" = T2.\"U_FO_ITEMID\" AND T0.\"U_FO_COMPONENTCOLORID\" = T2.\"U_FO_DIMENSIONID\"                         ";
            QueryComponenti += "               AND T2.\"U_FO_DIMENSIONTYPE\" = '1'             ";
            QueryComponenti += "           INNER JOIN \"@FO_COMMESSAPOSOUT\" T3 ON T3.\"U_FO_ITEMID\" = T0.\"U_FO_ITEMID\"                      ";
            QueryComponenti += "        INNER JOIN \"@FO_CLICOMMESSE\" T4 ON T3.\"U_FO_COMPANY\" = T4.\"U_FO_COMPANY\"  AND T4.\"U_TYPE\" = 'C'             ";
            QueryComponenti += "       WHERE             ";
            QueryComponenti += "           CONCAT(T3.\"U_FO_COMMESSA\",CONCAT('_', T3.\"U_FO_COMMESSAROW\")) = '" + SapParams.Values["commessaela"] + "'                           ";
            QueryComponenti += "           AND NOT EXISTS(             ";
            QueryComponenti += "                         SELECT TA.\"ItemCode\" FROM OITM TA                           ";
            QueryComponenti += "                           WHERE TA.\"ItemCode\" = T0.\"U_FO_COMPONENTID\"||'_0'||IFNULL(T0.\"U_FO_COMPONENTSIZEID\", '000')||'_'||IFNULL(T0.\"U_FO_COMPONENTCOLORID\", '0000')                   ";
            QueryComponenti += "                       )             ";
            QueryComponenti += "   )      ORDER BY \"ItemCode\", \"U_FO_LASTUPDATE\" DESC       ";
            return QueryComponenti;
        }

        public static string GetFiniti()
        {
            string QueryFiniti = "";
            //Query per estrarre i finiti
            QueryFiniti += " SELECT     ";
            QueryFiniti += "     DISTINCT(T0.\"U_FO_ITEMID\") AS \"ITEMID\",     ";
            QueryFiniti += " 	T0.\"U_FO_ITEMID\" || '_' || T0.\"U_FO_INVENTSIZEID\" || '_' || T0.\"U_FO_INVENTCOLORID\" AS \"ItemCode\",     ";
            QueryFiniti += " 	T0.\"U_FO_ITEMMODEL\" AS \"ITEMMODEL\",     ";
            QueryFiniti += " 	T0.\"U_FO_INVENTSIZEID\" AS \"INVENTSIZEID\",     ";
            QueryFiniti += " 	T0.\"U_FO_INVENTCOLORID\" AS \"INVENTCOLORID\",     ";
            QueryFiniti += " 	IFNULL(T3.\"U_FO_DIMENSIONNAME\", ' ') AS \"COLORDESC\",     ";
            QueryFiniti += " 	T0.\"U_FO_INVENTDROPID\" AS \"INVENTDROPID\",     ";
            QueryFiniti += " 	T0.\"U_FO_SIZEROW\" AS \"SIZEROW\",     ";
            QueryFiniti += " 	T0.\"U_FO_ITEMNAME\" || '   ' || IFNULL(T3.\"U_FO_DIMENSIONNAME\", ' ') AS \"ItemName\",     ";
            QueryFiniti += " 	T1.\"U_FO_INVENTSEASONID\" AS \"INVENTSEASONID\",     ";
            QueryFiniti += " 	T2.\"U_FO_UNITID\" AS \"UNITID\",     ";
            QueryFiniti += " 	T2.\"U_FO_PRODUCTCALSSID\" AS \"PRODUCTCALSSID\",     ";
            QueryFiniti += " 	T4.\"U_FO_CARDCODE\" AS \"CardCode\",     ";
            QueryFiniti += " 	T0.\"U_FO_COMPANY\" AS \"COMPANY\",     ";
            QueryFiniti += " 	T0.\"U_FO_LASTUPDATE\" AS \"LASTUPDATE\",     ";
            QueryFiniti += " 	T0.\"U_FO_TRANSACTIONID\" AS \"TRANSACTIONID\",     ";
            QueryFiniti += " 	T2.\"U_FO_BRANDID\" AS \"BRANDID\",     ";
            QueryFiniti += " 	T2.\"U_FO_ITEMTYPE\" AS \"ITEMTYPE\",     ";
            QueryFiniti += " 	T2.\"U_FO_PRODUCTGROUPID\" AS \"PRODUCTGROUPID\",     ";
            QueryFiniti += " 	T2.\"U_FO_TIPOLOGIADIFFICOLTA\" AS \"TIPOLOGIADIFFICOLTA\",     ";
            QueryFiniti += " 	T2.\"U_FO_FLAGETICHETTASERIALE\" AS \"FLAGETICHETTASERIALE\",     ";
            QueryFiniti += " 	T2.\"U_FO_LINEASTILE\" AS \"LINEASTILE\",     ";
            QueryFiniti += " 	T2.\"U_FO_DESCRIZIONELINEASTILE\" AS \"DESCRIZIONELINEASTILE\",     ";
            QueryFiniti += " 	T2.\"U_FO_CAD\" AS \"CAD\",     ";
            QueryFiniti += " 	T2.\"U_FO_FORNITORE\" AS \"FORNITORE\"     ";

            QueryFiniti += " FROM     ";
            QueryFiniti += "     \"@FO_COMMESSAPOSOUT\" T0     ";
            QueryFiniti += "     INNER JOIN \"@FO_COMMESSAHEADOUT\" T1 ON T0.\"U_FO_COMMESSA\" = T1.\"U_FO_COMMESSAID\"     ";
            QueryFiniti += "     INNER JOIN \"@FO_MASTERITEM\" T2 ON T0.\"U_FO_ITEMID\" = T2.\"U_FO_ITEMID\"     ";
            QueryFiniti += "     LEFT JOIN \"@FO_MASTERVARIANT\" T3 ON T0.\"U_FO_ITEMID\" = T3.\"U_FO_ITEMID\"  AND T0.\"U_FO_INVENTCOLORID\" = T3.\"U_FO_DIMENSIONID\" AND T3.\"U_FO_DIMENSIONTYPE\" = '1'     ";
            QueryFiniti += "     INNER JOIN \"@FO_CLICOMMESSE\" T4 ON T0.\"U_FO_COMPANY\" = T4.\"U_FO_COMPANY\"     ";
            QueryFiniti += " WHERE     ";
            QueryFiniti += "     CONCAT(T0.\"U_FO_COMMESSA\",CONCAT('_', \"U_FO_COMMESSAROW\")) = '" + SapParams.Values["commessaela"] + "'     ";
            QueryFiniti += "     AND T4.\"U_TYPE\" = 'C'     ";
            QueryFiniti += "     AND NOT EXISTS(     ";
            QueryFiniti += "          SELECT TA.\"ItemCode\" FROM OITM TA     ";
            QueryFiniti += "          WHERE TA.\"ItemCode\" = T0.\"U_FO_ITEMID\" || '_' || T0.\"U_FO_INVENTSIZEID\" || '_' || T0.\"U_FO_INVENTCOLORID\"     ";
            QueryFiniti += "          )     ";
            QueryFiniti += " AND T2.\"U_FO_LASTUPDATE\" = (SELECT MAX(TB.\"U_FO_LASTUPDATE\") FROM \"@FO_MASTERITEM\" TB WHERE T0.\"U_FO_ITEMID\" = TB.\"U_FO_ITEMID\") ";
            QueryFiniti += " AND T0.\"U_FO_TRANSACTIONID\" = (SELECT MAX(TB.\"U_FO_TRANSACTIONID\") FROM \"@FO_COMMESSAPOSOUT\" TB WHERE T0.\"U_FO_ITEMID\" = TB.\"U_FO_ITEMID\") ";
            return QueryFiniti;
        }

        public static string GetFasi()
        {
            string QueryFasi = "";
            QueryFasi += " SELECT       ";
            QueryFasi += "     DISTINCT T0.\"U_FO_SALESROUTEOPR\" as \"ItemCode\",       ";
            QueryFasi += " 	('Fase     ' || T0.\"U_FO_SALESROUTEOPR\") as \"ItemName\"       ";
            QueryFasi += " FROM       ";
            QueryFasi += "     \"@FO_COMMESSAREVOUT\" T0       ";
            QueryFasi += " WHERE CONCAT(T0.\"U_FO_COMMESSAID\", CONCAT('_', \"U_FO_COMMESSAROWID\")) = '" + SapParams.Values["commessaela"] + "'       ";
            QueryFasi += "    AND NOT EXISTS(       ";
            QueryFasi += "         SELECT TA.\"ItemCode\" FROM OITM TA       ";
            QueryFasi += "         WHERE TA.\"ItemCode\" = T0.\"U_FO_SALESROUTEOPR\"       ";
            QueryFasi += "     )       ";
            return QueryFasi;
        }

        public static string GetDistinte()
        {
            string QueryDiBa = "";
            QueryDiBa += " SELECT       ";
            QueryDiBa += "     DISTINCT(T0.\"U_FO_ITEMID\" || '_' || IFNULL(T0.\"U_FO_INVENTSIZERID\", 'U') || '_' || T0.\"U_FO_INVENTCOLORID\") AS \"DibaCode\",       ";
            QueryDiBa += "     IFNULL(T0.\"U_FO_INVENTSIZERID\", 'U') AS \"Taglia\",        ";
            QueryDiBa += "     T0.\"U_FO_ITEMID\" AS \"ItemID\",        ";
            QueryDiBa += "     T0.\"U_FO_INVENTCOLORID\" AS \"ColorID\"       ";
            QueryDiBa += " FROM       ";
            QueryDiBa += "     \"@FO_MASTERMATERIAL\" T0       ";
            QueryDiBa += "     INNER JOIN \"@FO_COMMESSAPOSOUT\" T1 ON T0.\"U_FO_ITEMID\" = T1.\"U_FO_ITEMID\"       ";
            QueryDiBa += " WHERE       ";
            QueryDiBa += "     CONCAT(T1.\"U_FO_COMMESSA\", CONCAT('_', T1.\"U_FO_COMMESSAROW\")) = '" + SapParams.Values["commessaela"] + "'       ";
            QueryDiBa += "     AND NOT EXISTS(       ";
            QueryDiBa += "           SELECT TA.\"Code\" FROM OITT  TA      ";
            QueryDiBa += "           WHERE TA.\"Code\" = T0.\"U_FO_ITEMID\" || '_' || IFNULL(T0.\"U_FO_INVENTSIZERID\", 'U') || '_' || T0.\"U_FO_INVENTCOLORID\"       ";
            QueryDiBa += "           )        ";
            return QueryDiBa;
        }

        public static string GetFasiDIBA(string DibaQuery)
        {
            string QueryFasi = "";
            QueryFasi += "SELECT DISTINCT (";
            QueryFasi += "    \"U_FO_REFOPR\") AS \"Fase\" , MAX(\"U_FO_NOMEFILE\") AS \"NomeFile\" ";
            QueryFasi += "FROM";
            QueryFasi += "    \"@FO_MASTERMATERIAL\"";
            QueryFasi += "WHERE \"U_FO_ITEMID\"||'_'||IFNULL(\"U_FO_INVENTSIZERID\",'U')||'_'||\"U_FO_INVENTCOLORID\" ='" + DibaQuery + "'";
            QueryFasi += "GROUP BY \"U_FO_REFOPR\" ORDER BY \"Fase\" ASC";
            return QueryFasi;
        }

        public static string GetArticoliDIBA(string Fase, string NomeFile, string DibaQuery)
        {
            string QueryArt = "";
            QueryArt += "SELECT ";
            QueryArt += "    \"U_FO_ITEMID\"||'_'||IFNULL(\"U_FO_INVENTSIZERID\",'U')||'_'||\"U_FO_INVENTCOLORID\" AS \"DibaCode\",";
            QueryArt += "    \"U_FO_COMPONENTID\"||'_0'||IFNULL(\"U_FO_COMPONENTSIZEID\",'000')||'_'||IFNULL(\"U_FO_COMPONENTCOLORID\",'0000') AS \"ItemCode\",";
            QueryArt += "    \"U_FO_BOMQTY\" AS \"Qty\",";
            QueryArt += "    \"U_FO_GOODSPROPERTY\" AS \"Prop\",";
            QueryArt += "    \"U_FO_REFOPR\" AS \"Fase\",";
            QueryArt += "    \"U_FO_COMPANY\" AS \"Company\",";
            QueryArt += "    \"U_FO_INVENTDROPID\" AS \"Drop\",";
            QueryArt += "    \"U_FO_INVENTVERSIONID\" AS \"Versione\",";
            QueryArt += "    \"U_FO_INVENTSEASONID\" AS \"Stagione\",";
            QueryArt += "    \"U_FO_INVENTTHICKNESS\" AS \"Spessore\",";
            QueryArt += "    \"U_FO_INVENTCARAT\" AS \"Caratura\"";
            QueryArt += "FROM";
            QueryArt += "    \"@FO_MASTERMATERIAL\"";
            QueryArt += "WHERE \"U_FO_ITEMID\"||'_'||IFNULL(\"U_FO_INVENTSIZERID\",'U')||'_'||\"U_FO_INVENTCOLORID\" ='" + DibaQuery + "'";
            QueryArt += "AND \"U_FO_REFOPR\" = '" + Fase + "' AND \"U_FO_NOMEFILE\" = '" + NomeFile + "' ";
            QueryArt += "ORDER BY";
            QueryArt += "    \"U_FO_ITEMID\"||'_'||IFNULL(\"U_FO_INVENTSIZERID\",'U')||'_'||\"U_FO_INVENTCOLORID\", \"U_FO_REFOPR\", \"Code\"";
            return QueryArt;
        }

        public static string GetMagazzinoDIBA(string Company, string Componente, string FaseSub, string Fase)
        {
            string Magazzino = "";
            if (Company == "06") //Se gucci
            {
                if ((Componente == "000411" || Componente == "00Y411") && FaseSub == "1")
                {
                    Magazzino = "MpefoGU";
                }
                else if ((Componente == "000511" || Componente == "00Y511") && Fase == "333")
                {
                    Magazzino = "MmodGU";
                }
                else if (Fase == "444")
                {
                    Magazzino = "MpackGU";
                }
                else if (Componente == "000311")
                {
                    Magazzino = "MprGU";
                }
                else
                {
                    Magazzino = "MZ-GUCCI";
                }
            }
            else if (Company == "33") //Se YSL
            {
                if ((Componente == "000411" || Componente == "00Y411") && FaseSub == "1")
                {
                    Magazzino = "MpefoYSL";
                }
                else if ((Componente == "000511" || Componente == "00Y511") && Fase == "333")
                {
                    Magazzino = "MmodYSL";
                }
                else if (Fase == "444")
                {
                    Magazzino = "MPAGYSL ";
                }
                else if (Componente == "000311")
                {
                    Magazzino = "MprYSL";
                }
                else
                {
                    Magazzino = "MZ-YSL";
                }
            }

            if (Magazzino == "")
            {
                Magazzino = "Mc";
            }
            return Magazzino;
        }

        public static string GetMagazzinoDIBATable(string GoodSProp, string Company, string Componente, string Fase)
        {
            string QueryMag = "";

            QueryMag += " SELECT                        ";
            QueryMag += "     IFNULL(\"C1\", IFNULL(\"C3\", IFNULL (\"C2\", \"C4\")))                        ";
            QueryMag += " FROM                        ";
            QueryMag += " (                        ";
            QueryMag += "     SELECT                        ";
            QueryMag += "          (                        ";
            QueryMag += "             SELECT TOP 1 T0.\"U_FO_MAG\" AS \"Mag1\"                        ";
            QueryMag += "             FROM \"@FO_MAGIMPORT\" T0                         ";
            QueryMag += "             WHERE T0.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'AND T0.\"U_FO_COMPANY\" = '" + Company + "' AND T0.\"U_FO_COMPONENTE\" = '" + Componente + "'                         ";
            QueryMag += "                 AND '" + Fase + "' IN(T0.\"U_FO_FASE\") ) AS \"C1\",                        ";
            QueryMag += "         (                        ";
            QueryMag += "             SELECT TOP 1 T1.\"U_FO_MAG\" AS \"Mag2\"                        ";
            QueryMag += "             FROM \"@FO_MAGIMPORT\" T1                         ";
            QueryMag += "             WHERE T1.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'AND T1.\"U_FO_COMPANY\" = '" + Company + "' AND '" + Fase + "' IN (T1.\"U_FO_FASE\") ) AS \"C2\",                        ";
            QueryMag += "         (                        ";
            QueryMag += "             SELECT TOP 1 T2.\"U_FO_MAG\" AS \"Mag3\"                        ";
            QueryMag += "             FROM \"@FO_MAGIMPORT\" T2                         ";
            QueryMag += "             WHERE T2.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'AND T2.\"U_FO_COMPANY\" = '" + Company + "' AND T2.\"U_FO_COMPONENTE\" = '" + Componente + "') AS \"C3\",                        ";
            QueryMag += "         (                        ";
            QueryMag += "             SELECT TOP 1 T3.\"U_FO_MAG\" AS \"Mag4\"                        ";
            QueryMag += "             FROM \"@FO_MAGIMPORT\" T3 WHERE T3.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'                        ";
            QueryMag += "                 AND T3.\"U_FO_COMPANY\" = '" + Company + "' AND T3.\"U_FO_COMPONENTE\"  IS NULL AND T3.\"U_FO_FASE\" IS NULL ) AS \"C4\"	                        ";
            QueryMag += "     FROM DUMMY                        ";
            QueryMag += " )                        ";

            /*
                QueryMag += " SELECT IFNULL(TA.\"U_FO_MAG\", IFNULL(TB.\"U_FO_MAG\", IFNULL( TC.\"U_FO_MAG\", TD.\"U_FO_MAG\" ))) AS \"Mag\" ";
                QueryMag += " FROM ( SELECT T0.\"U_FO_MAG\" FROM \"@FO_MAGIMPORT\" T0 WHERE T0.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'  ";
                QueryMag += "        AND T0.\"U_FO_COMPANY\" = '" + Company + "' AND T0.\"U_FO_COMPONENTE\" = '" + Componente + "' AND T0.\"U_FO_FASE\" IN (" + Fase + ") ) TA, ";
                QueryMag += "      ( SELECT T1.\"U_FO_MAG\" FROM \"@FO_MAGIMPORT\" T1 WHERE T1.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'  ";
                QueryMag += "        AND T1.\"U_FO_COMPANY\" = '" + Company + "' AND T1.\"U_FO_FASE\" IN (" + Fase + ") ) TB, ";
                QueryMag += "      ( SELECT T2.\"U_FO_MAG\" FROM \"@FO_MAGIMPORT\" T2 WHERE T2.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'  ";
                QueryMag += "        AND T2.\"U_FO_COMPANY\" = '" + Company + "' AND T2.\"U_FO_COMPONENTE\" = '" + Componente + "' ) TC, ";
                QueryMag += "      ( SELECT T3.\"U_FO_MAG\" FROM \"@FO_MAGIMPORT\" T3 WHERE T3.\"U_FO_GOODSPROPERTY\" = '" + GoodSProp + "'  ";
                QueryMag += "        AND T3.\"U_FO_COMPANY\" = '" + Company + "' AND T3.\"U_FO_COMPONENTE\"  IS NULL AND T3.\"U_FO_FASE\" IS NULL) TD ";
            */
            return QueryMag;
        }

        public static string GetOrdine()
        {
            string QueryOrd = " ";
            QueryOrd += " SELECT        ";
            QueryOrd += "   T0.\"U_FO_COMMESSAID\" || '_' || T1.\"U_FO_COMMESSAROW\" AS \"Ordine\",        ";
            QueryOrd += "   TO_DATE(SUBSTRING(T1.\"U_FO_DLVDATE\", 0, 10), 'DD/MM/YYYY') AS \"Date\",        ";
            QueryOrd += "   T3.\"U_FO_CARDCODE\" AS \"CardC\",        ";
            QueryOrd += "   MAX(T1.\"U_FO_NOMEFILE\") AS \"NomeFile\",        ";
            QueryOrd += "   T0.\"U_FO_MASTERORDERID\" AS \"MASTERORDERID\",        ";
            QueryOrd += "   T3.\"U_FO_COMPANY\" AS \"COMPANY\"        ";
            QueryOrd += " FROM        ";
            QueryOrd += "   \"@FO_COMMESSAHEADOUT\" T0        ";
            QueryOrd += "   INNER JOIN \"@FO_CLICOMMESSE\" T3 ON T0.\"U_FO_COMPANY\" = T3.\"U_FO_COMPANY\"        ";
            QueryOrd += "   INNER JOIN \"@FO_COMMESSAPOSOUT\" T1 on T1.\"U_FO_COMMESSA\" = T0.\"U_FO_COMMESSAID\"        ";
            QueryOrd += " WHERE        ";
            QueryOrd += "   T3.\"U_TYPE\" = 'C' AND T0.\"U_FO_COMMESSAID\" || '_' || T1.\"U_FO_COMMESSAROW\" = '" + SapParams.Values["commessaela"] + "'        ";
            QueryOrd += " GROUP BY T0.\"U_FO_COMMESSAID\" || '_' || T1.\"U_FO_COMMESSAROW\", T1.\"U_FO_DLVDATE\", T3.\"U_FO_CARDCODE\",T0.\"U_FO_MASTERORDERID\",T3.\"U_FO_COMPANY\"  ";
            return QueryOrd;
        }

        public static string GetArticoliOrdine(string NomeFile)
        {
            string QueryArt = "";
            QueryArt += " SELECT        ";
            QueryArt += "      T1.\"U_FO_ITEMID\" || '_' || IFNULL(T1.\"U_FO_INVENTSIZEID\",'U') || '_' || IFNULL(T1.\"U_FO_INVENTCOLORID\",0000) AS \"ItemCode\",        ";
            QueryArt += " 	 CAST(CAST(REPLACE(T1.\"U_FO_SALESQTY\", ',', '.') AS DOUBLE) AS INTEGER) AS \"Qty\"        ";
            QueryArt += " FROM        ";
            QueryArt += "     \"@FO_COMMESSAPOSOUT\" T1        ";
            QueryArt += " WHERE        ";
            QueryArt += "      T1.\"U_FO_COMMESSA\" || '_' || T1.\"U_FO_COMMESSAROW\" = '" + SapParams.Values["commessaela"] + "'  AND T1.\"U_FO_NOMEFILE\" = '" + NomeFile + "'       ";
            return QueryArt;
        }

        public static string GetFasiOrdine(string NomeFile, string ItemCode)
        {
            string QueryFasi = "";
            QueryFasi += "  SELECT       ";
            QueryFasi += "  	 DISTINCT(T2.\"U_FO_SALESROUTEOPR\") AS \"Fase\" ,       ";
            QueryFasi += "       CAST (CAST (REPLACE (T1.\"U_FO_SALESQTY\", ',', '.') AS DOUBLE) AS INTEGER) AS \"Qty\" ,       ";
            QueryFasi += "       CAST (REPLACE (T2.\"U_FO_PRICE\", ',', '.') AS DOUBLE) AS \"UNITPRZ\" ,        ";
            QueryFasi += "       CAST (REPLACE (T2.\"U_FO_LINEAMOUNT\", ',', '.') AS DOUBLE) AS \"LINEPRZ\"        ";
            QueryFasi += "  FROM        ";
            QueryFasi += "  	\"@FO_COMMESSAHEADOUT\" T0        ";
            QueryFasi += "      INNER JOIN \"@FO_COMMESSAPOSOUT\" T1 on T1.\"U_FO_COMMESSA\" = T0.\"U_FO_COMMESSAID\"        ";
            QueryFasi += "      INNER JOIN \"@FO_COMMESSAREVOUT\" T2 on T2.\"U_FO_COMMESSAID\" = T0.\"U_FO_COMMESSAID\"        ";
            QueryFasi += "          AND T1.\"U_FO_ITEMID\"||'_'||IFNULL(T1.\"U_FO_INVENTSIZEID\", 'U')||'_'||IFNULL(T1.\"U_FO_INVENTCOLORID\",0000) = T2.\"U_FO_ITEMID\"||'_'||IFNULL(T2.\"U_FO_INVENTSIZEID\",'U')||'_'||IFNULL(T2.\"U_FO_INVENTCOLORID\",0000 )  ";
            QueryFasi += "  WHERE        ";
            QueryFasi += "      T1.\"U_FO_COMMESSASPLITROW\" = T2.\"U_FO_COMMESSASPLITROWID\"        ";
            QueryFasi += "      AND T0.\"U_FO_COMMESSAID\"||'_'||T1.\"U_FO_COMMESSAROW\" = '" + SapParams.Values["commessaela"] + "'       ";
            QueryFasi += "      AND T1.\"U_FO_ITEMID\"||'_'||T1.\"U_FO_INVENTSIZEID\"||'_'||T1.\"U_FO_INVENTCOLORID\" = '" + ItemCode + "'        ";
            QueryFasi += "      AND T1.\"U_FO_NOMEFILE\" = '" + NomeFile + "' ";
            return QueryFasi;
        }

        public static string GetDDT()
        {
            string QueryDDT = "";
            QueryDDT += " SELECT    ";
            QueryDDT += "     DISTINCT(T0.\"U_FO_PACKINGSLIPID\") AS \"NumAtCard\",    ";
            QueryDDT += "     MAX(T0.\"U_FO_NOMEFILE\") AS \"NomeFile\",     ";
            QueryDDT += "     T0.\"U_FO_PACKINGSLIPDATE\" AS \"DocDate\",    ";
            QueryDDT += "     T1.\"U_FO_CARDCODE\" AS \"CardCode\"    ";
            QueryDDT += " FROM    ";
            QueryDDT += " 	\"@FO_DNPOSIN\" T0     ";
            QueryDDT += "     INNER JOIN \"@FO_CLICOMMESSE\" T1 ON T0.\"U_FO_COMPANY\" = T1.\"U_FO_COMPANY\"     ";
            QueryDDT += " WHERE    ";
            QueryDDT += "     T1.\"U_TYPE\" = 'S' AND NOT EXISTS (    ";
            QueryDDT += "         SELECT TA.\"NumAtCard\" FROM ODLN TA WHERE TA.\"NumAtCard\" = T0.\"U_FO_PACKINGSLIPID\"    ";
            QueryDDT += "         UNION ALL    ";
            QueryDDT += "        SELECT TB.\"NumAtCard\" FROM ODRF TB WHERE TB.\"NumAtCard\" = T0.\"U_FO_PACKINGSLIPID\" AND TB.\"ObjType\" = '20')     ";
            QueryDDT += "      AND YEAR(T0.\"U_FO_PACKINGSLIPDATE\") IN (" + SapParams.Values["anni_commesse"] + ")";
            QueryDDT += " GROUP BY T0.\"U_FO_PACKINGSLIPID\", T0.\"U_FO_PACKINGSLIPDATE\", T1.\"U_FO_CARDCODE\" ";
            return QueryDDT;
        }

        public static string GetArticoliDDT(string CodiceDDT, string NomeFile)
        {
            string QueryArt = "";
            QueryArt += " SELECT        ";
            QueryArt += "      T0.\"U_FO_ITEMID\"||'_0'||IFNULL(T0.\"U_FO_INVENTSIZEID\", '000')||'_'||IFNULL(T0.\"U_FO_INVENTCOLORID\", '0000') as \"ItemCode\",        ";
            QueryArt += "      T0.\"U_FO_QTY\" AS \"Qty\"        ";
            QueryArt += " FROM        ";
            QueryArt += " 	\"@FO_DNPOSIN\" T0         ";
            QueryArt += " WHERE        ";
            QueryArt += " 	\"U_FO_PACKINGSLIPID\" = '" + CodiceDDT + "' AND IFNULL(T0.\"U_FO_INVENTSIZEID\", '000') <> 'U' AND T0.\"U_FO_NOMEFILE\" = '" + NomeFile + "' ";
            QueryArt += " UNION ALL SELECT        ";
            QueryArt += "      T0.\"U_FO_ITEMID\"||'_'||IFNULL(T0.\"U_FO_INVENTSIZEID\", '0000')||'_'||IFNULL(T0.\"U_FO_INVENTCOLORID\", '0000') as \"ItemCode\",        ";
            QueryArt += "      T0.\"U_FO_QTY\" AS \"Qty\"        ";
            QueryArt += " FROM        ";
            QueryArt += " 	\"@FO_DNPOSIN\" T0         ";
            QueryArt += " WHERE        ";
            QueryArt += " 	\"U_FO_PACKINGSLIPID\" = '" + CodiceDDT + "' AND T0.\"U_FO_INVENTSIZEID\" = 'U'  AND T0.\"U_FO_NOMEFILE\" = '" + NomeFile + "' ";
            return QueryArt;
        }

        public static string InsRowParams(string Tabella, string CodeParam, string CommParam)
        {
            string Sequence = Tabella + "_S";
            string QueryIns = "INSERT INTO \"" + Tabella + "\" (\"Code\",\"Name\",\"U_FO_NAME\", \"U_FO_VALUE\", \"U_FO_COMM\")";
            QueryIns += " VALUES ( \"" + Sequence + "\".NEXTVAL, \"" + Sequence + "\".CURRVAL, '" + CodeParam + "','','" + CommParam + "' )  ";
            return QueryIns;
        }

        public static string GetMagOrdine(string Company)
        {
            string Query = "SELECT \"U_FO_MAG\" FROM \"@FO_MAGIMPORT\" WHERE \"U_FO_COMPANY\" = '"+Company+"' AND \"U_FO_FASE\" = 'Ordine' ";
            return Query;
        }
    }
}
