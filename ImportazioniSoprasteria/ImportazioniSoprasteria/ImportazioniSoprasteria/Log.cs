﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImportazioniSoprasteria
{
    public class Log
    {

        public static void LogBB(string TestoIn)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = SapParams.Values["pathlog"] + "LOG_BB/LOG_" + SapParams.Values["database"] + "_" + dt.ToString("yyyyMMdd") + ".txt";

            //SE PRIMA RIGA
            if (!File.Exists(logFile))
            {
                using (StreamWriter w = File.AppendText(logFile))
                {
                    w.WriteLine(" LOG ERRORE IMPORTAZIONI " + dt.ToString("dd/MM/yyyy HH:mm:ss"));
                }
            }


            using (StreamWriter w = File.AppendText(logFile))
            {
                string PreText = "";
                if (SapParams.Values["commessaela"] != "")
                {
                    PreText = "Errore Commessa " + SapParams.Values["commessaela"];
                }
                w.WriteLine(PreText + " - " + TestoIn);
            }

            Console.WriteLine(TestoIn);
            LogGenerale("LOG BB:" + TestoIn);
        }

        public static void LogSoprasteria(string TestoIn)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = SapParams.Values["pathlog"] + "LOG_SOPRASTERIA/LOG_" + SapParams.Values["database"] + "_" + dt.ToString("yyyyMMdd") + ".txt";

            //SE PRIMA RIGA
            if (!File.Exists(logFile))
            {
                using (StreamWriter w = File.AppendText(logFile))
                {
                    w.WriteLine(" LOG ERRORE IMPORTAZIONI " + dt.ToString("dd/MM/yyyy HH:mm:ss"));
                }
            }


            using (StreamWriter w = File.AppendText(logFile))
            {
                string PreText = "";
                if (SapParams.Values["commessaela"] != "")
                {
                    PreText = "Errore Commessa " + SapParams.Values["commessaela"];
                }
                w.WriteLine(PreText + " - " + TestoIn);
            }

            Console.WriteLine(TestoIn);
            LogGenerale("LOG SOPRASTERIA:" + TestoIn);
        }

        public static void LogGenerale(string TestoIn)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = SapParams.Values["pathlog"] + "LOG_GEN/LOG_" + SapParams.Values["database"] + "_" + dt.ToString("yyyyMMdd") + ".txt";

            using (StreamWriter w = File.AppendText(logFile))
            {
                w.WriteLine(TestoIn);
            }

            Console.WriteLine(TestoIn);
        }

        public static void LogGeneraleWUParams(string DB, string Path, string TestoIn)
        {
            DateTime dt = new DateTime();
            dt = DateTime.Now;
            string logFile = Path + "LOG_GEN/LOG_" + DB + "_" + dt.ToString("yyyyMMdd") + ".txt";

            using (StreamWriter w = File.AppendText(logFile))
            {
                w.WriteLine(TestoIn);
            }

            Console.WriteLine(TestoIn);
        }
    }
}
