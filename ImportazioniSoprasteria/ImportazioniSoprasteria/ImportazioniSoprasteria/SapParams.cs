﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ImportazioniSoprasteria
{
    public class SapParams
    {
        public static IDictionary<string, string> Values = new Dictionary<string, string>();

        public static bool ConnessioneSAP(XmlDocument docXml, ref SAPbobsCOM.Company oCompany)
        {
            string PathLog = "";
            string DB = "";
            try
            {
               
                oCompany = new SAPbobsCOM.Company();
                //Leggo i parametri di connessione dal file xml
                XmlNodeList elemList = docXml.GetElementsByTagName("DB");
                foreach (XmlNode node in elemList)
                {
                    DB = node.Attributes["CompanyDB"].Value;
                    PathLog= node.Attributes["UrlLog"].Value;

                    oCompany.Server = node.Attributes["Server"].Value;
                    //oCompany.LicenseServer = node.Attributes["License"].Value;
                    oCompany.DbUserName = node.Attributes["DbUserName"].Value;
                    oCompany.DbPassword = node.Attributes["DbPassword"].Value;
                    oCompany.CompanyDB = node.Attributes["CompanyDB"].Value;
                    oCompany.UserName = node.Attributes["UserName"].Value;
                    oCompany.Password = node.Attributes["Password"].Value; 
                }
                oCompany.DbServerType = SAPbobsCOM.BoDataServerTypes.dst_HANADB;
                oCompany.UseTrusted = false;

                int errCode = oCompany.Connect();

                if (errCode != 0)
                {
                    string err;
                    int code;
                    oCompany.GetLastError(out code, out err);
                    Log.LogGeneraleWUParams(DB, PathLog,"   Connessione database " + DB + " in errore: " + code + " - " + err);
                    return false;
                }
                else
                {
                    Log.LogGeneraleWUParams(DB, PathLog, "   Connessione al database " + DB + ": OK ");
                }
            }
            catch (Exception e)
            {
                Log.LogGeneraleWUParams(DB, PathLog, "Errore Connessione Database" + e.Message.ToString());
                return false;
            }
            
            return true;
        }

        public static void DisconnesioneSAP(ref SAPbobsCOM.Company oCompany)
        {
            oCompany.Disconnect();
            oCompany = null;
            Log.LogGenerale("   Disconnessione dal database : OK");
        }

        public static bool GetParams(XmlDocument docXml, ref SAPbobsCOM.Company oCompany)
        {

            SAPbobsCOM.IRecordset oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            string TabellaParametriC = "@FO_IMP_SOPRASTERIA";
            string TabellaParametri = "\"@FO_IMP_SOPRASTERIA\"";
            string CodeParam = ""; string CommParam = "";
            bool AddPar = false;

            #region Parametri da XML
                XmlNodeList elemList = docXml.GetElementsByTagName("DB");
                Values["database"] = elemList[0].Attributes["CompanyDB"].Value;
                Values["pathlog"] = elemList[0].Attributes["UrlLog"].Value;
                Values["commessaela"] = "";
            #endregion

            #region Anni Commesse (falcoltativi)

            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'EffettuaControlloAnno'");
            if (oRecord.RecordCount>0)
            {
                Values["ctrl_anno_commesse"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "EffettuaControlloAnno";
                CommParam = "Valori: Y/N -> SE Y il codice selezionerà solo le commesse degli anni inseriti nel parametro ANNO_COMMESSE ";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
            }

            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'ANNO_COMMESSE' ");
            if (oRecord.RecordCount>0)
            {
                Values["anno_commesse"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "ANNO_COMMESSE";
                CommParam = "Indica gli anni delle commesse da importare, è possibile inserire più anni separati da virgola ";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
            }
            Values["anni_commesse"] = "";
            #endregion

            #region Gruppo Articoli [REQUIRED]
            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'GruppoARTDef'");
            if (oRecord.RecordCount>0)
            {
                Values["grp_art_dft"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "GruppoARTDef";
                CommParam = "Gruppo articoli con cui viene inserito un nuovo articolo se non viene recuperato dal PRODUCTCALSSID  ";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
            }


            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'GruppoARTSemi'");
            if (oRecord.RecordCount > 0)
            {
                if (oRecord.Fields.Item(0).Value.ToString() == "")
                {
                    Log.LogBB("ERRORE, Gruppo Articoli Semilavorato non recuperato dalla tabella di configurazione!");
                    return false;
                }
                Values["grp_art_semi"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "GruppoARTSemi";
                CommParam = "OBBLIGATORIO Gruppo articoli con cui viene inserito un nuovo articolo Semilavorato   ";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
                AddPar = true;
            }

            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM " + TabellaParametri + " WHERE \"U_FO_NAME\" = 'GruppoARTFiniti'");
            if (oRecord.RecordCount > 0)
            {
                if (oRecord.Fields.Item(0).Value.ToString() == "")
                {
                    Log.LogBB("ERRORE, Gruppo Articoli Finiti non recuperato dalla tabella di configurazione!");
                    return false;
                }
                Values["grp_art_finiti"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "GruppoARTFiniti";
                CommParam = "OBBLIGATORIO Gruppo articoli con cui viene inserito un nuovo articolo Finito   ";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
                AddPar = true;
            }

            #endregion

            #region Magazzino Default  [REQUIRED]
            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'MagDefault'");
            if (oRecord.RecordCount>0)
            {
                if (oRecord.Fields.Item(0).Value.ToString() == "")
                {
                    Log.LogBB("ERRORE, Magazzino Default non recuperato dalla tabella di configurazione!");
                    return false;
                }
                Values["mag_default"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "MagDefault";
                CommParam = "OBBLIGATORIO Magazzino usato per inserimenti distinte base nel caso non venga recuperato il mag specifico per la riga";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
                AddPar = true;
            }
            
            #endregion

            #region Magazzino Articoli Controllo  [REQUIRED]
            oRecord.DoQuery("SELECT \"U_FO_VALUE\" FROM "+ TabellaParametri+" WHERE \"U_FO_NAME\" = 'MagArtCtrl'");
            if (oRecord.RecordCount > 0)
            {
                if (oRecord.Fields.Item(0).Value.ToString() == "")
                {
                    Log.LogBB("ERRORE, Magazzino Articoli Controllo non recuperato dalla tabella di configurazione!");
                    return false;
                }
                Values["mag_art_ctrl"] = oRecord.Fields.Item(0).Value.ToString();
            }
            else
            {
                CodeParam = "MagArtCtrl";
                CommParam = "OBBLIGATORIO Magazzino usato per inserimenti articoli controllo in distinta base";
                oRecord.DoQuery(Query.InsRowParams(TabellaParametriC, CodeParam, CommParam));
                AddPar = true;
            }
            #endregion

            if (AddPar)
            {
                Log.LogBB("Alcune parametrizzazioni non sono state trovate, aggiungere i parametri in tabella " + TabellaParametri + " e riprovare");
                return false;
            }

            return true;
        }

        public static void GestisciErrore(ref SAPbobsCOM.Company oCompany, int Typeid, string ObjID, string ExtraMessage = "")
        {
            /*
             Type:
             1 Articoli Componenti
             2 Articoli Finiti
             3 Fasi Ordine
             4 Distinte Base
             5 Ordine Cliente
             6 Numero di Catalogo
             7 Entrata Merci
             8 Articolo Semilavorato
            */
            string ErrorDesc;
            int ErrorCode;
            oCompany.GetLastError(out ErrorCode, out ErrorDesc);
            string CommessaID = Values["commessaela"];
            string ErrorMsg = "";
            string BaseMsg = "Errore elaborazione commessa " + CommessaID + " -->";
            switch (Typeid)
            {
                case 1:
                    if (ErrorCode == -5002 && ObjID == "")
                    {
                        ErrorMsg += "Errore Inserimento Articolo Componente, il campo ITEMID è vuoto , errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(BaseMsg + ErrorMsg);
                        Log.LogSoprasteria(BaseMsg + ErrorMsg);
                    }
                    else
                    {
                        ErrorMsg = "Errore Inserimento Articolo Componente : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(ErrorMsg);
                    }

                    break;

                case 2:
                    if (ErrorCode == -5002 && ObjID == "")
                    {
                        ErrorMsg += "Errore Inserimento Articolo Finito,  errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(BaseMsg + ErrorMsg);
                        Log.LogSoprasteria(BaseMsg + ErrorMsg);
                    }
                    else
                    {
                        ErrorMsg = "Errore Inserimento Articolo Finito : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(ErrorMsg);
                    }

                    break;

                case 3:
                    ErrorMsg = "Errore Inserimento Fase : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                    Log.LogBB(ErrorMsg);
                    break;

                case 4:
                    if (ErrorCode == -2028)
                    {
                        ErrorMsg = "Errore Inserimento Distinta Base: " + ObjID + " un articolo o una fase non sono stati trovati su SAP, errore " + ErrorCode + " - " + ErrorDesc;
                        Log.LogSoprasteria(ErrorMsg);
                        Log.LogBB(ErrorMsg);
                    }
                    else
                    {
                        ErrorMsg = "Errore Inserimento Distinta Base : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(ErrorMsg);
                    }
                    break;


                case 5:

                    if (ErrorCode == -2028)
                    {
                        ErrorMsg = "Errore Inserimento Ordine Cliente : " + ObjID + ", uno o più articoli non sono stati trovati su sap errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogSoprasteria(ErrorMsg);
                        Log.LogBB(ErrorMsg);
                    }
                    else
                    {
                        ErrorMsg = "Errore Inserimento Ordine Cliente : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(ErrorMsg);
                    }
                    break;

                case 6:
                    ErrorMsg = "Errore Inserimento Numero di Catalogo BP : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                    Log.LogBB(ErrorMsg);
                    break;

                case 7:

                    if (ErrorCode == -2028)
                    {
                        ErrorMsg = "Errore Inserimento Entrata Merci: " + ObjID + " un articolo o una fase non sono stati trovati su SAP ";
                        Log.LogSoprasteria(ErrorMsg);
                        Log.LogBB(ErrorMsg);
                    }
                    else
                    {
                        ErrorMsg = "Errore Inserimento Entrata Merci : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                        Log.LogBB(ErrorMsg);
                    }
                    break;

                case 8:
                    ErrorMsg = "Errore Inserimento Semilavorato : " + ObjID + ", errore n: " + ErrorCode + " - " + ErrorDesc;
                    Log.LogBB(ErrorMsg);
                    break;

                default:
                    break;
            }

        }

        public static string ControllaArticolo(string ItemCode, ref SAPbobsCOM.Company oCompany)
        {
            SAPbobsCOM.IRecordset oRecord = oCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
            oRecord.DoQuery("SELECT \"ItemCode\" FROM OITM WHERE \"ItemCode\" = '" + ItemCode + "'");
            if (oRecord.RecordCount == 0)
            {
                Log.LogBB(" ERRORE, Articolo " + ItemCode + " non trovato su SAP");
                Log.LogSoprasteria(" ERRORE, Articolo " + ItemCode + " non trovato su SAP");
            }

            return ItemCode;
        }


    }
}
